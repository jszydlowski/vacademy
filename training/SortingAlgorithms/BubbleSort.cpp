#include <iostream>
#include <vector>
#include <algorithm>

void bubblesort(int* table, int size) {

	using std::cout;
	using std::endl;
	
	for(int i = 0; i < size; ++i) {
		cout << table[i] << endl;
	}
	cout << endl << endl;

	for(int i = 0; i < size-1; ++i) {
		for(int j = 0; j < size-1-i; ++j) {
			if(table[j] > table[j+1]) {
				int x = table[j];
				table[j] = table[j+1];
				table[j+1] = x;
			}
		}
	}

	for(int i = 0; i < size; ++i) {
		cout << table[i] << endl;
	}
	
}

void bubblesort(std::vector<int> &v) {
	using std::cout;
	for(auto it = v.begin(); it != v.end()-1; ++it) {
		if(*it > *(it+1)) {
			std::iter_swap(it, it+1);
		}
	}

	for(auto &&i : v) {
		cout << i << "\n";
	}
}

void selectionsort(std::vector<int> &v) {
	for(auto i = v.begin(); i != v.end(); ++i) {
		std::iter_swap(i, std::min_element(i, v.end()));
	}

	for(auto &&i : v) {
		std::cout << i << "\n";
	}
}

int main() {
	std::cout << "START\n";
	std::vector<int> vi{52, 34, 89, 17, 10};
	int tab[5] = {52, 34, 89, 17, 10};

//	bubblesort(tab, 5);
//	bubblesort(vi);	
	selectionsort(vi);

	for(auto &&i : vi) {
		std::cout << i << "\n";
	}

	std::cout << "\nEND\n";
	return 0;
}
