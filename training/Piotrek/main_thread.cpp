//
//  main_thread.cpp
//  Threads
//
//  Created by PiotrekGawronski on 09.10.2017.
//  Copyright © 2017 PiotrekGawronski. All rights reserved.
//

#include <iostream>
#include <thread>
#include <chrono>
#include <ctime>

static bool isLoopRunning {true};
const char EXIT_KEY {'q'};
const int SECONDS_PER_PRINT {3};

/**
 * @brief printTime -> method that counts time and prints actual date with time.
 * When elapsed time is equal or greater than SECONDS_PER_PRINT, function will
 * print actual date with time and then the timer will reset itself.
 */
void printTime()
{
    auto startTimer = std::chrono::high_resolution_clock::now();

    while (isLoopRunning)
    {
        if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - startTimer).count() >= SECONDS_PER_PRINT)
        {
            auto timeNow {std::chrono::system_clock::now()};

            std::time_t timeNowT = {std::chrono::system_clock::to_time_t(timeNow)};

            std::cout << std::ctime(&timeNowT) << std::endl;

            startTimer = std::chrono::high_resolution_clock::now();
        }
    }
}

/**
 * @brief getInputFromUser -> function that gets input from user.
 * The function is in infinite loop, as long as user inputs 'q' (quick), then
 * this function and printTime() function will end up looping.
 */
void getInputFromUser()
{
    char userInput {' '};

    while (isLoopRunning)
    {
        std::cin >> userInput;

        if (userInput == EXIT_KEY)
        {
            isLoopRunning = false;
        }
    }
}

int main(int argc, const char * argv[]) {

    std::thread printingThread(printTime);
    std::thread userInputThread(getInputFromUser);

    printingThread.join();
    userInputThread.join();

    return 0;
}
