
#include <vector>
#include <iostream>
#include <cmath>

const std::vector<int> coin_types{100, 50, 10, 5, 2, 1};

std::vector<int> get_change(float cash, float price) {
	using std::cout;
	using std::endl;

	int icash = floor(cash*100);
	int iprice = floor(price*100);

	std::vector<int> coins(6, 0);

	int rest = icash - iprice;
	cout << "Rest: " << rest/100.0 << endl;

	int ind = 0;
	float temp{0.0};
	for(auto &&i : coin_types) {
		coins[ind] = rest / i;
		rest -= coins[ind] * coin_types[ind];
		cout << coin_types[ind]/(float)100 << "$\t x " << coins[ind] << " (actuall rest = " << rest/(float)100 << ")" << endl;
		++ind;
	}
	cout << "Rest on end: " << rest/float(100) << endl;
	cout << endl;
}

int main() {
	using std::cout;
	using std::endl;

	get_change(2.17, 1.05);	

	get_change(10.0, 1.71);


	return 0;
}
