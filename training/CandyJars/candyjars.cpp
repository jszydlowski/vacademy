#include <cassert>
#include <iostream>

struct Data {
	unsigned a;
	unsigned b;
	unsigned k;
};
unsigned fill(unsigned N, unsigned M, Data* data) {
	assert(data);
	assert(N >= 3 && N <= 10000000);
	assert(M >= 1 && M <= 10000);
	
	unsigned result = 0;

	for(int i = 0; i < M; ++i) {
		assert(data[i].a >= 1);
		assert(data[i].b >= data[i].a);
		assert(N >= data[i].b);
		assert(data[i].k >= 0 && data[i].k <= 1000000);

		result += ((data[i].b - data[i].a + 1) * data[i].k);
	}

	result = result / N;

	return result;
}

int main() {
	
	// Example data
	Data d[3];
	d[0] = {1, 2, 100};
	d[1] = {2, 5, 100};
	d[2] = {3, 4, 100};

	std::cout << fill(5, 3, d);
	assert( fill(5, 3, d) == 160);


	return 0;
}
