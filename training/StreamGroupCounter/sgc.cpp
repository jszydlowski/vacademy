#include <iostream>
#include <cstdint>
#include <cassert>
#include <cstring>

enum class StreamGroupType {
	lower_case,
	upper_case,
	digit,
	null_terminator,
	undefined
};

constexpr 
StreamGroupType GetCharStreamGroupType(char a_character) noexcept {
	StreamGroupType result = StreamGroupType::undefined;

	if( (a_character >= 'a') && (a_character <= 'z') ) {
		result = StreamGroupType::lower_case;
	} else if( (a_character >= 'A') && (a_character <= 'Z') ) {
		result = StreamGroupType::upper_case;
	} else if( (a_character >= '0') && (a_character <= '9') ) {
		result = StreamGroupType::digit;
	} else if( a_character == 0 ) {
		result = StreamGroupType::null_terminator;
	} else {
		result = StreamGroupType::undefined;
	}

	return result;
}

int_fast32_t StreamGroupCounter(char a_character) noexcept {


	static int_fast32_t static_groupNumber = 0;
	static StreamGroupType static_lastGroupType = StreamGroupType::null_terminator;

	StreamGroupType currentGroupType = GetCharStreamGroupType(a_character);
	assert(currentGroupType != StreamGroupType::undefined);

	if(currentGroupType != static_lastGroupType) {
		if(currentGroupType == StreamGroupType::null_terminator) {
			static_groupNumber = 0;
		}
		++static_groupNumber;
		static_lastGroupType = currentGroupType;
	}
	return static_groupNumber;
}

void Test_StreamGroupCounter();

int main() {
	std::cout << "START" << std::endl;

	Test_StreamGroupCounter();

	std::cout << "END\n";
	return 0;
}

//* Runtime tests ////////////////////////////////////////////////////////////////////////////////
void Test_StreamGroupCounter() {
	char testStr[] = "AABBaabb1212dede123AaBCDcdc1a";
	int testExpected[] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 8, 8, 9, 9, 9, 10, 11};

	std::cout << "Test start (" << testStr << ")\n";
	for(int i = 0; i < strlen(testStr); ++i) {
		std::cout << testStr[i] << " = " << StreamGroupCounter(testStr[i]) << " = " << testExpected[i] << "\n";
		assert( StreamGroupCounter(testStr[i]) == testExpected[i] );	
	}
	
}	

// *//////////////////////////////////////////////////////////////////////////////////////////////



//* Static tests /////////////////////////////////////////////////////////////////////////////////

// GetCharStreamGroupType
static_assert( GetCharStreamGroupType('a') == StreamGroupType::lower_case, "test 1" );
static_assert( GetCharStreamGroupType('Z') == StreamGroupType::upper_case, "test 2" );
static_assert( GetCharStreamGroupType('8') == StreamGroupType::digit, "test 3" );
static_assert( GetCharStreamGroupType('$') == StreamGroupType::undefined, "test 4" );
static_assert( GetCharStreamGroupType(0) == StreamGroupType::null_terminator, "test 5" );

// *//////////////////////////////////////////////////////////////////////////////////////////////


