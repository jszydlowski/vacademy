### STREAM GROUP COUNTER ###

Write a function, which receives a character and returns in which group the character belongs. 
The group is continuous sequence of characters of one and the same type � capital letters, lower letters or numbers (i.e. we have only 3 types of groups). 
The function is supposed to be called every time a new character appears. If a zero-termination character is received, the group counter is reset.

## Example: ##

# Input: #
 AABBaabb1212dede123AaBCDcdc1a
 1
     2
         3
             4
      	         5
	      		    6
                     7
                      8
                         9
                           10
                            11

# Output: #
A is in group 1 => function returns 1
B is in group 1 => function returns 1
A is in group 1 => function returns 1
b is in group 2 => function returns 2
a is in group 2 => function returns 2
1 is in group 3 => function returns 3
2 is in group 3 => function returns 3
3 is in group 3 => function returns 3
A is in group 4 => function returns 4
a is in group 5 => function returns 5
