#pragma once
#include <utility>

using Point = std::pair<int, int>;

class Drawable {
public:

protected:
    int size{0};
    Point pos{0,0};

    bool visible{false};

public:
    virtual void Draw() = 0;
//    virtual void MoveTo(int x, int y) = 0;
//    virtual void MoveTo(Point pos) = 0;
//    virtual void MoveBy(int dx, int dy) = 0;
//    virtual void MoveBy(Point deltapos) = 0;

};



