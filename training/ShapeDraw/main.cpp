#include "Drawable.hpp"
#include "Rectangle.hpp"

int main() {
    Rectangle r{Point{8, 42}, 5};
    r.Draw();

    return 0;
}