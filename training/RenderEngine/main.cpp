#include <iostream>

#include "engine.h"

int main (int argc, char* argv[])
{
    int array[] {1, 2, 3, 4, 5};

    for (int &number : array)
    {
        number += 1;
        std::cout << number << std::endl;
    }

    return 0;
}
