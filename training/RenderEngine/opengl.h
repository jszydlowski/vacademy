#ifndef OPENGL_H
#define OPENGL_H

#include <iostream>

#include "lib.h"

class OpenGL : public Lib
{
public:
    OpenGL();
    OpenGL(std::string inputText);
    void render();

private:
    std::string m_text;
};

#endif // OPENGL_H
