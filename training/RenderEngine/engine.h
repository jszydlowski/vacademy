#ifndef ENGINE_H
#define ENGINE_H

#include "opengl.h"

class Engine
{
public:
    Engine();

    void operator << (std::string text)
    {
        OpenGL openGL(text);
        openGL.render();
    }
};

#endif // ENGINE_H
