// VAcademy
// Lesson 02 - Operators

#include <stdio.h>
#include <stdlib.h>

int crazy_add(int n) {
	n += n + n++ + ++n;
	return n;
}

int crazy_bitwise(int n) {
	return n | 9 & 17 ;
	// 00011 
	// 01001 |
	//=
	// 10001 &
	//=
}

bool getFalse() {
	printf("getFalse() call\n");
	return false;
}

bool getTrue() {
	printf("getTrue() call\n");
	return true;
}

bool crazy_bitwise2(bool b) {
	return b || getFalse() && getTrue();
}

bool crazy_bitwise3(bool b) {
	return getTrue() && getFalse() || b;
}

void ptr_increment1() {
	int arr[] = {10, 20};
	int *p = arr;
	++*p;
	printf("arr[0] = %d, arr[1] = %d, *p = %d\n", arr[0], arr[1], *p);
}

void ptr_increment2() {
	int arr[] = {10, 20};
	int *p = arr;
	*p++;
	printf("arr[0] = %d, arr[1] = %d, *p = %d\n", arr[0], arr[1], *p);
}

void ptr_increment3() {
	int arr[] = {10, 20};
	int *p = arr;
	*++p;
	printf("arr[0] = %d, arr[1] = %d, *p = %d\n", arr[0], arr[1], *p);
}

int main() {
	system("CLS");	
	printf("(1) = %d\n", crazy_add(1));
	system("PAUSE"); printf("\n\n");
	printf("(2) = %d\n", crazy_bitwise(3));

	system("PAUSE"); printf("\n\n");
	printf("(3) = %d\n", crazy_bitwise2(false));
	system("PAUSE"); printf("\n\n");
	printf("(4) = %d\n", crazy_bitwise3(false));

	system("PAUSE"); printf("\n\n");
	printf("(5) = %d\n", crazy_bitwise2(true));
	system("PAUSE"); printf("\n\n");
	printf("(6) = %d\n", crazy_bitwise3(true));


	system("PAUSE"); printf("\n\n");
	ptr_increment1();
	system("PAUSE"); printf("\n\n");
	ptr_increment2();
	system("PAUSE"); printf("\n\n");
	ptr_increment3();

}
