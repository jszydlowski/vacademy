
#include <stdio.h>

//int main() {
	
//}

void c_example() {

	printf("Test");

	int x = 5;

	for(int i = 0; i < 5; ++i) {
		x += i;
	}
}

int main() {
	const int MY_CONST_INT = 42;
	const int* my_const_int_prt = &MY_CONST_INT;
	int* my_notconst_int_prt = (int*)(void*)my_const_int_prt;
	printf("MY_CONST_INT\t\t (addr=%p val=%d)\n", &MY_CONST_INT, MY_CONST_INT); 
	printf("my_notconst_int_prt\t (addr=%p val=%d)\n", my_notconst_int_prt, *my_notconst_int_prt);

	printf("--------------------------------------------------\n");
	*my_notconst_int_prt = 8;

	printf("MY_CONST_INT\t\t (addr=%p val=%d)\n", &MY_CONST_INT, MY_CONST_INT); 
	printf("my_notconst_int_prt\t (addr=%p val=%d)\n", my_notconst_int_prt, *my_notconst_int_prt);

	return 0;
}
