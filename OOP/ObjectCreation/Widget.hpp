
class Widget {
public:
	Widget();
	~Widget();

	Widget(int data);

	Widget(const Widget& rhs);				// Copy ctor
	Widget operator=(const Widget& rhs);	// Copy = operator

	Widget(const Widget&& rhs);				// Move ctor
	Widget operator=(const Widget&& rhs);	// Move = operator

private:
	int intData{0};

public:
	void printIntData();
};


