#include "Widget.hpp"

#include <iostream>

Widget::Widget() {
	std::cout << "Widget ctor\n";
}

Widget::~Widget() {
	std::cout << "Widget dtor\n";
}

Widget::Widget(int data) : intData(data) {
	std::cout << "Widget ctor (data=" << data << ")\n";
}

Widget::Widget(const Widget& rhs) {
	std::cout << "Widget copy ctor\n";
	this->intData = rhs.intData;
}

Widget Widget::operator=(const Widget& rhs) {
	std::cout << "Widget copy = ctor\n";
	return rhs;
}

void Widget::printIntData() {
	std::cout << "intData = " << intData << "\n";
}



