# README #

VAcademy C++ training
author: __Jacek 'jsec' Szydlowski

## Lessons topics ##
1. Difference between C and C++ & C++ Standards
2. Operators
3. Buildin Types and scope
4. C++ Language keywords
5. Pointers & arrays & containers
6. Const correctness
7. Signed/unsigned missmatch
8. Class/Object
9. Encapsulation
10. Ctor/Dtor
11. Operator overloading
12. Friendship
13. Inheritance/Polymorphism
14. Subtopic
15. Templates
16. Exception handling

