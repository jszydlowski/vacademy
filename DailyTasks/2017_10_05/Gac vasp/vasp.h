#ifndef VASP_H
#define VASP_H

#include "data.h"
//


class Vasp
{
public:

    Vasp();
    Vasp(Data *data);
    Vasp(const Vasp&) = delete;
    ~Vasp();

    Data* operator->();
    Data& operator*();
    void operator=(const Vasp&) = delete;


private:
    Data* pointer;
};

#endif // VASP_H
