#include <iostream>
#include <data.h>
#include <vasp.h>

#include <memory>

//using namespace std;
using std::cout;

int main()
{
    Data data{1 , 1.4 , 'a'};
   Vasp ptr(&data);

    cout<<data.a << std::endl;
    cout<<data.b << std::endl;
    cout<<data.c << std::endl;

//    Vasp ptr(&data);

    cout<< ptr->a << std::endl;
    cout<< ptr->b << std::endl;
    cout<< ptr->c << std::endl;

//    std::unique_ptr<Data> ptr;

//    cout << ptr.get()->a << std::endl;

    return 0;
}
