#include "vasp.h"
#include <qdebug>


Vasp::Vasp()
{
    pointer = nullptr;
}

Vasp::Vasp(Data *data) :
    pointer(data)
{
//    pointer = data;
}


Data* Vasp::operator->()
{
    return pointer;
}

Data& Vasp::operator*()
{
    return *pointer;
}

Vasp::~Vasp()
{
    delete pointer;

}
