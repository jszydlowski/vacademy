#include "Vasp.h"
#include "Data.h"
#include "vasptests.h"

#include <memory>

int main(int argc, const char * argv[])
{
    Vasp x;

    x->m_name = "a";

    std::cout << x->m_name << std::endl;

    return 0;
}
