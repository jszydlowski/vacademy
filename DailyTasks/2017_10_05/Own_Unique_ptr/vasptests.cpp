#include "vasptests.h"

#include <cstdlib>
#include <iostream>
#include <memory>

VaspTests::VaspTests()
{

}

void VaspTests::nullptrAsArgument()
{
    Vasp testVasp(nullptr);

    testVasp = new Data("Hello", "world!");

    std::unique_ptr<Data> testOriginal = nullptr;

    testOriginal = std::unique_ptr<Data>(new Data("Hello", "world!"));

    if (testVasp->m_name == testOriginal.get()->m_name)
    {
        std::cout << "TEST PASS" << std::endl;
    }
    else
    {
        std::cout << "TEST FAIL" << std::endl;
    }
}
