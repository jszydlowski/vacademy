#ifndef Data_h
#define Data_h

#include <iostream>

struct Data
{
    std::string m_name;
    std::string m_surname;
    
    Data() :
        m_name(""),
        m_surname("")
    {
        
    }
    
    Data(const std::string &name, const std::string &surname) :
        m_name(name),
        m_surname(surname)
    {
        
    }
};

#endif /* Data_h */
