#ifndef Vasp_h
#define Vasp_h

#include "Data.h"

/**
 * @brief The Vasp class -> Class that simulates unique_ptr.
 */
class Vasp
{
public:
    /**
     * @brief Vasp -> constructor that creates instance
     * of vasp and sets it as nullptr.
     */
    Vasp();
    /**
     * @brief Vasp -> constructor that creates instance
     * of vasp and sets given argument to m_dataRawPtr.
     * @param data
     */
    Vasp(Data *data);

    Vasp(const Vasp&) = delete;

    ~Vasp();
    
    /**
     * @brief operator * -> operator * is now used to
     * return value of m_dataRowPtr;
     * @return -> value of m_dataRowPtr;
     */
    Data& operator* ();
    /**
     * @brief operator = -> operator "=" will remove actual object of
     * m_dataRowPtr and sets new object for him.
     * @param data -> instance of Data.
     */
    void operator= (Data *data);

    void operator= (const Vasp&) = delete;

    /**
     * @brief operator -> operator "->" allows us to directly call
     * an attribute of Data.
     * @return
     */
    Data* operator-> ();
    
private:
    /**
     * @brief m_dataRawPtr -> normal pointer for Data objects.
     */
    Data *m_dataRawPtr;
    /**
     * @brief releasePtr -> method that releases m_dataRowPtr and
     * remove reference from reference counter.
     */
    void releasePtr();
};

#endif /* Vasp_h */
