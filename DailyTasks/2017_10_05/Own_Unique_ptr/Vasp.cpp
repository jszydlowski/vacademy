#include "Vasp.h"

Vasp::Vasp () :
    m_dataRawPtr(nullptr)
{

}

Vasp::Vasp (Data *data) :
    m_dataRawPtr(data)
{

}

Vasp::~Vasp ()
{ 
    releasePtr();
}

Data& Vasp::operator* ()
{
    return *m_dataRawPtr;
}

void Vasp::operator= (Data *data)
{
    delete m_dataRawPtr;

    m_dataRawPtr = data;
}

Data* Vasp::operator-> ()
{
    if (m_dataRawPtr == nullptr)
    {
//        std::cout << "ERROR" << std::endl;

        m_dataRawPtr = new Data();
    }

    return m_dataRawPtr;
}

void Vasp::releasePtr ()
{
    delete m_dataRawPtr;
    m_dataRawPtr = nullptr;
}
