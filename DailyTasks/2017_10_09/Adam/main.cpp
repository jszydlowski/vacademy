#include <iostream>
#include <vector>
#include <qdebug>

using std::vector;


void funsort(vector<int> &numbers);
void funsort2(vector<int> &numbers);

int main()
{
    vector<int> numbers {1, -2, 4, 7, -5, 9, -6, 2, 3};

    funsort2(numbers);

    for (auto i : numbers)
    {
        std::cout << i << std::endl;
    }



    return 0;
}
void funsort(vector<int> &numbers)
{
    vector<int> sortVector;
    vector<int> noNegativeVector;
    //vector<int> oryginal = numbers;


    for(int i = 0; i < numbers.size() ; i++)
    {
        if(numbers[i] >= 0)
        {
            noNegativeVector.push_back(numbers[i]);
        }
    }

    std::sort(noNegativeVector.begin(), noNegativeVector.end()); //sort positive numbers

    int noNegativeIter = 0;


    for(size_t i = 0 ; i < numbers.size() ; i++)
    {
        if(numbers[i] < 0)
        {
            sortVector.push_back(numbers[i]);
        }
        else
        {
            sortVector.push_back(noNegativeVector[noNegativeIter]);
            noNegativeIter++;
        }
    }

    numbers = sortVector;
}
void funsort2(vector<int> &numbers)
{
    for (int i = 0; i < numbers.size(); ++i)
    {
        for (int j = 0; j < numbers.size(); ++j)
        {
            if (numbers[i] < 0)
            {
                continue;
            }
            else if (numbers[i] < numbers[j])
            {
                int temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
            }
        }
    }



}
