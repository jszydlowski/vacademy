#include <iostream>
#include <vector>
#include <algorithm>

/**
 * @brief excludeMinus - function excludes all negative numbers from an array
 * @param array
 * @param position
 * @param number
 */
void excludeMinus(std::vector < int > &array, std::vector <int> &position, std::vector < int > &number);

/**
 * @brief eraseSorted - deletes all negative numbers from original array.
 *                      Since we created another vector with number of negatives, we know how many we want to delete.
 * @param array
 * @param position
 */
void eraseSorted(std::vector < int > &array, std::vector <int> &position);

/**
 * @brief addMinusNumbers - Adds selected negative numbers to the original array using saved position of and exact numbers provied by number vector.
 * @param array
 * @param position
 * @param number
 */
void addMinusNumbers(std::vector < int > &array, std::vector <int> &position, std::vector < int > &number);

/**
 * @brief sortVector standard sorting function, ascending.
 * @param array
 */
void sortVector(std::vector <int> &array);

/**
 * @brief printVector - It ptints our vector
 * @param array
 */
void printVector(std::vector < int > &array);


int main(int argc, char *argv[])
{  
    std::vector < int > array = {3, -1, 7, 2, 1, -1};
    std::vector < int > position;
    std::vector < int > number;

    excludeMinus(array, position, number);
    sortVector(array);
    eraseSorted(array, position);
    addMinusNumbers(array, position, number);

    printVector(array);

}

void excludeMinus(std::vector < int > &array, std::vector <int> &position, std::vector < int > &number)
{
    for (int i=0; i<array.size(); i++)
    {
        if(array[i] < 0)
        {
            position.push_back(i);
            number.push_back(array[i]);
        }
    }
}

void eraseSorted(std::vector < int > &array, std::vector <int> &position)
{
    for(int i=0; i<position.size();i++)
    {
        array.erase(array.begin());
    }
}

void addMinusNumbers(std::vector < int > &array, std::vector <int> &position, std::vector < int > &number)
{
     for(int i = 0; i<position.size(); i++)
     {
         array.insert(array.begin() + position[i], number[i]);
     }
}

void sortVector(std::vector <int> &array)
{
    std::sort(array.begin(), array.end());
}

void printVector(std::vector < int > &array)
{
    for (int i = 0; i< array.size() ; i++)
    {
        std::cout<<array[i];
    }
}
