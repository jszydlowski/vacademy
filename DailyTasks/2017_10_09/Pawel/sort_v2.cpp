#include <iostream>
#include <string>
#include <vector>

int main()
{
  std::vector<int> table = {-1, 5, 0, 11,  1, -7, 2, -8};

  for (int i = 0; i < table.size(); i++)
  {
      for (int j = 0; j < table.size(); j++)
      {
          if (table[i] < 0) // if negative number is found, skip it.
          {
              continue;
          }

          else if (table[i] < table[j]) // if table[i] is smaller than table[j], make a swap.
          {
        	  table[i] ^= table[j];
              table[j] ^= table[i];
              table[i] ^= table[j];
          }
      } // second loop

  } // first loop

  for(int i = 0; i < table.size(); i++)
  {
	  std::cout << table[i] << ", ";
  }
}
