#include <iostream>
#include <vector>
#include <algorithm>

/*
 * func - vector intow (sort rosna, jesli ujemne, nie sortujemy ujemnych liczb)
 * i.e. INPUT:  3, -1, 7, 2, 1, -1
 *      OUTPUT: 1, -1, 2, 3, 7, -1
 */

void sortWithoutNegative(std::vector<int> &table);

int main()
{
    std::vector<int> table = {8, 1, -1, 5, 2, -1, 5, 2};

    sortWithoutNegative(table);

	return 0;
}

void sortWithoutNegative(std::vector<int> &table)
{
    std::vector<int> position;
    std::vector<int> number;

    // save position and value of negative number
    for(int i = 0; i < table.size(); i++)
    {
        if(table[i] < 0)
        {
        	position.push_back(i);
        	number.push_back(table[i]);
        }
    }

    // sort vector of integers
    sort(table.begin(), table.end());


    // delete first n negative numbers
    for(int i = 0; i < position.size(); i++)
    {
        table.erase(table.begin());
    }

    // add negative numbers using saved position and value
    for(int i = 0; i < position.size(); i++)
    {
        table.insert( table.begin() + position[i], number[i]);
    }

    // print vector
	for(int i = 0; i < table.size(); i++)
	{
		std::cout << table[i] << ", ";
    }
}
