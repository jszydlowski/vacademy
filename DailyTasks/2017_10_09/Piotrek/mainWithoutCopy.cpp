#include <iostream>
#include <algorithm>
#include <vector>

void sortVector (std::vector<int> &data)
{
    for (unsigned i = 0; i < data.size(); ++i)
    {
        for (unsigned j = 0; j < data.size(); ++j)
        {
            if (data[i] < 0)
            {
                continue;
            }
            if (data[i] < data[j])
            {
                std::swap(data[i], data[j]);
            }
        }
    }

    for (auto element : data)
    {
        std::cout << element << std::endl;
    }
}

int main()
{
    std::vector<int> m_data {3, -1, 7, 2, 1, -1};
    std::vector<int> m_anotherData {4, -1, 7, -2, 88, 4};

    std::cout << "BEFORE SORT" << std::endl;
    std::cout << "---------------------" << std::endl;

    for (auto element : m_data)
    {
        std::cout << element << std::endl;
    }

    std::cout << "\nAFTER SORT" << std::endl;
    std::cout << "---------------------" << std::endl;

    sortVector(m_data);

    std::cout << "\n---------------------" << std::endl;
    std::cout << "---------------------" << std::endl;

    std::cout << "\nBEFORE SORT" << std::endl;
    std::cout << "---------------------" << std::endl;

    for (auto element : m_anotherData)
    {
        std::cout << element << std::endl;
    }

    std::cout << "\nAFTER SORT" << std::endl;
    std::cout << "---------------------" << std::endl;

    sortVector(m_anotherData);

    std::cout << "" << std::endl;

    return 0;
}
