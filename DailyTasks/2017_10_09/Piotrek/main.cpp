#include <iostream>
#include <vector>
#include <algorithm>

//vector intow, funkcja, ktora go przyjmuje. Chcemy je posortowac rosnaco, pomiędzy tymi liczbami beda liczby ujemne, maja zostac w tych miejsach, w ktorych sa

void sortVector (std::vector<int> &data)
{
    auto dataCopy = data;

    for (int element = 0; element < data.size(); ++element)
    {
        if (data[element] < 0)
        {
            data.erase(data.begin() + element);
        }
    }

    std::sort(data.begin(), data.end());

    for (int element = 0; element < dataCopy.size(); ++element)
    {
        if (dataCopy[element] < 0)
        {
            data.insert(data.begin() + element, dataCopy[element]);
        }
    }

    for (int element : data)
    {
        std::cout << element << std::endl;
    }
}

int main()
{
    std::vector<int> m_data {3, -1, 7, 2, 1, -1};

    sortVector(m_data);

    return 0;
}
