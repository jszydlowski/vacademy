#include "petermaster.h"

#include <iostream>

int PeterMaster::m_createdObjectsCounter = 0;

PeterMaster::PeterMaster() :
    m_isClassNameTrue(true)
{
    ++m_createdObjectsCounter;

    std::cout << "Default constructor" << std::endl;
    std::cout << "Number of objects created so far: " << m_createdObjectsCounter << std::endl << std::endl;
}

PeterMaster::PeterMaster(const bool &responseToClassName) :
    m_isClassNameTrue(responseToClassName)
{
    ++m_createdObjectsCounter;

    std::cout << "Constructor with arguments" << std::endl;
    std::cout << "Number of objects created so far: " << m_createdObjectsCounter << std::endl << std::endl;
}

PeterMaster::PeterMaster(const PeterMaster &object)
{
    this->m_isClassNameTrue = object.m_isClassNameTrue;

    ++m_createdObjectsCounter;

    std::cout << "Copy constructor" << std::endl;
    std::cout << "Number of objects created so far: " << m_createdObjectsCounter << std::endl << std::endl;
}

PeterMaster::~PeterMaster()
{
    --m_createdObjectsCounter;
    std::cout << "Number of object that survived: " << m_createdObjectsCounter << std::endl << std::endl;
}

void PeterMaster::doWork()
{
    static bool isThisFirstMethodCall = true;

    if (isThisFirstMethodCall)
    {
        std::cout << "Doing work" << std::endl << std::endl;
        isThisFirstMethodCall = false;
    }
    else
    {
        std::cout << "Work already done" << std::endl << std::endl;
    }
}
