#ifndef PETERMASTER_H
#define PETERMASTER_Hb

/**
 * @brief The PeterMaster class -> class that show usage of three types of constructor.
 * First one is constructor with no parameters.
 * Second one is constructor with parameter.
 * Third one is copy constructor.
 *
 * Class is also counting how many object has been created already.
 */
class PeterMaster
{
public:
    /**
     * @brief PeterMaster -> Default constructor.
     * Initializes class boolean member as true,
     * increment objects counter
     * and prints some information.
     */
    PeterMaster();
    /**
     * @brief PeterMaster -> Constructor with argument.
     * Initializes class boolean member with given parameter,
     * also increment objects counter
     * and prints some information.
     *
     * @param responseToClassName -> Value given by user.
     */
    PeterMaster(const bool &responseToClassName);
    /**
     * @brief PeterMaster -> Copy constructor.
     * Initialize new object with other object data.
     * @param object
     */
    PeterMaster(const PeterMaster &object);
    ~PeterMaster();

    /**
     * @brief doWork -> Method that prints different output
     * according to the value of static variable inside method.
     */
    void doWork();

private:
    /**
     * @brief m_isClassNameTrue -> Variable that says if class name is right.
     */
    bool m_isClassNameTrue;
    /**
     * @brief m_createdObjectsCounter -> Static variable that holds number
     * of already exsisting objects.
     */
    static int m_createdObjectsCounter;
};

#endif // PETERMASTER_H
