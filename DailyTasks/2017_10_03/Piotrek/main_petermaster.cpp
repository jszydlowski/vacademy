#include "petermaster.h"

int main()
{
    PeterMaster master;
    PeterMaster master2(false);
    PeterMaster master3(master2);

    master.doWork();
    master2.doWork();
    master3.doWork();

    return 0;
}
