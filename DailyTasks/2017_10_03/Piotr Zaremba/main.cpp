#include <iostream>

using std::cout;
using std::endl;

static int counter;

class PiotrZaremba
{
public:
    PiotrZaremba()
    {
        counter++;
        cout << "Default constructor: " << counter<<endl;
    }
    PiotrZaremba(const PiotrZaremba &jacek)
    {
        counter++;
        cout << "Copying constructor: " <<counter<< endl;
    }
    PiotrZaremba(int number)
    {
        counter++;
        cout << "Constructor with one argument: " <<counter<< endl;
    }
    PiotrZaremba(int number, int telephone)
    {
        counter++;
        cout << "Constructor with many arguments: " <<counter<< endl;
    }

    ~PiotrZaremba()
    {
        counter--;
        cout << "Destructor, decreasing counter!: " <<counter<< endl;
    }
};

void function(PiotrZaremba object)
{
    counter++;
    cout << "Converting constructor: "<<counter<<endl;
}

int main(int argc, char *argv[])
{ 
    int var = 5;
    function(var);
    PiotrZaremba domyslnyPiotr;
    PiotrZaremba pojedynczyPiotr(2);
    PiotrZaremba wielokrotnyPiotr(2, 125345);
    PiotrZaremba kopiujacyPiotr(domyslnyPiotr);

}
