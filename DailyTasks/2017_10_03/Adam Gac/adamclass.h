#ifndef ADAMCLASS_H
#define ADAMCLASS_H



class AdamClass
{
public:
    AdamClass();
    AdamClass(int umber);
    AdamClass(AdamClass &classToCopy);

    void doWork();
    ~AdamClass();

    static int m_objectCounter;
private:

    int m_exampleNumber;
    bool m_statusMethod = false;
};

#endif // ADAMCLASS_H
