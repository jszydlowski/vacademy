#include "adamclass.h"
#include "qdebug"

int AdamClass::m_objectCounter = 0;

AdamClass::AdamClass()
{
    ++m_objectCounter;
    qDebug() << "AdamClass()";
    qDebug() << "creted:" << m_objectCounter << "objects";

}

AdamClass::AdamClass(int number)
{
    ++m_objectCounter;
    m_exampleNumber = number;
    qDebug() << "AdamClass(int m_exampleNumber)";
    qDebug() << "creted:" << m_objectCounter << "objects";
}

AdamClass::AdamClass(AdamClass &classToCopy)
{
     ++m_objectCounter;
    this->m_exampleNumber = classToCopy.m_exampleNumber;
    this->m_statusMethod = classToCopy.m_statusMethod;
    qDebug() << "AdamClass(AdamClass &classToCopy)";
    qDebug() << "creted:" << m_objectCounter << "objects";
}

void AdamClass::doWork()
{
    if(m_statusMethod == false)
    {
        qDebug() << "Doing work";
        m_statusMethod = true;
    }
    else
    {
        qDebug() << "Work already done";
    }
}

AdamClass::~AdamClass()
{
    --m_objectCounter;
    qDebug() << "left" << m_objectCounter<< "objects";
}
