#include <cstdint>

class Jsec{
public:
	Jsec();									// ctor
	~Jsec();								// dtor
//	Jsec(const Jsec& rhs);					// copy ctor
//	Jsec& operator=(const Jsec& rhs);		// copy assignment operator
	
//	Jsec(Jsec&& rhs);						// move ctor
//	Jsec& operator=(Jsec&& rhs);			// move assignment operator

	void doWork();	
private:
	static int64_t count;
};
