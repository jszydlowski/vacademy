#include <iostream>
using std::cout;
using std::endl;

static int counter;
bool       semafor = 1;

class PawelSzydelko
{
private:
    double cheesburger;
    double chips;
    float  cola;

public:
        PawelSzydelko() // konstruktor domyslny
        {
        	counter++;
        	cheesburger = 1;
        	chips = 1;
        	cola = 1;
        	cout << "Konstruktor domyslny " << counter << endl;
        }

        PawelSzydelko(const PawelSzydelko &nthg) // konstruktor kopiujacy
        {
        	counter++;
        	cheesburger = nthg.cheesburger + 1;
        	chips = 1;
        	cola = 1;
            cout << "konstruktor kopiujacy " << counter <<endl;
        }

        PawelSzydelko(int aa) // konstruktor z jednym argumentem formalnym
        {
        	counter++;
        	cheesburger = aa;
        	chips = 1;
        	cola = 1;
            cout << "konstruktor z jednym argumentem "<< counter << endl;
        }

        PawelSzydelko(int aa, int bb, int cc) // konstruktor z wieloma argumentami
        : cheesburger(aa), // lista inicjalizacyjna konstruktora
		  chips(bb),
		  cola(cc) // zmiana wartosci stalej
        {
        	counter++;
        	cout << "konstruktor z wieloma argumentami "<< counter << endl;
        }

        PawelSzydelko(int aa, int bb): PawelSzydelko(1)
        {
			counter++;
        	cout << "Konstruktor delegujacy " << counter << endl;
        }

        ~PawelSzydelko()
        {
        	counter--;
        	cout << "destruktor" << counter << endl;
        }

        void DoWork()
        {
        	if(semafor)
        	{
        		cout << "done" << endl;
        		semafor = 0;
        	}
        	else
        	{
        		cout << "already done" << endl;
        	}
        }

}psObj; // tworzenie pierwszego obiektu przed wywolaniem main

int main()
{

	PawelSzydelko psObj2(2); // jeden arg
	psObj2.DoWork();
	PawelSzydelko psObj3(3,3,3); // wiele arg
	psObj3.DoWork();
	PawelSzydelko psObj4(3,3); // delegujacy
	PawelSzydelko psObj5(psObj3); // kopiujacy
	psObj5.DoWork();

}
