Task description:
	Create a class named after Your name and place it in this folder. Class
	should create all possible constructors in that way, that every creation
	of instance will:
		A) count each object created
		B) print out what kind of constructor was called
		C) print out number of object created so far
	Additionally destructor should also be created in that way, that every
	object destroyed will:
		A) decrease count of objects created
		B) print out number of objects left after destruction of this one
	Class should also haev one method called doWork that takes no parameters
	and returns no value. Method should print out "Doing work" when called but
	only if work was not done yet. If work was already done, method should
	print out "Work already done.".
	Code need to be clean, 100% in english, and should be commented.
	All created files need to be pushed to this repo.	

