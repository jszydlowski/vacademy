#include "sebastian.h"

int Sebastian::m_objetsCounter = 0;
bool Sebastian::m_workDone = false;

Sebastian::Sebastian()
{
    m_objetsCounter++;

    cout << "Default constructor called" << endl;
    cout << "Number of objects after creation: " << m_objetsCounter << endl;
}

Sebastian::Sebastian(int number, char character) :
    m_number(number), m_character(character)
{
    m_objetsCounter++;

    cout << "Constructor with parameters called" << endl;
    cout << "Number of objects after creation: " << m_objetsCounter << endl;
}

Sebastian::Sebastian(const Sebastian &copy) :
    m_number(copy.m_number), m_character(copy.m_character)
{
    m_objetsCounter++;

    cout << "Copy constructor called" << endl;
    cout << "Number of objects after creation: " << m_objetsCounter << endl;
}

Sebastian::~Sebastian()
{
    m_objetsCounter--;

    cout << "Number of objects after destruction: " << m_objetsCounter << endl;
}

void Sebastian::doWork()
{
    if (!m_workDone)
    {
        m_workDone = true;
        cout << "Doing work" << endl;
    }
    else
    {
        cout << "Work already done" << endl;
    }
}
