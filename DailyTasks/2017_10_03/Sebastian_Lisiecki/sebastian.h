#ifndef SEBASTIAN_H
#define SEBASTIAN_H

#include <iostream>

using namespace std;

class Sebastian
{
public:
    // Default constructor
    Sebastian();

    // Constructor with parameters
    Sebastian(int number, char character);

    // Copy constructor
    Sebastian(const Sebastian &copy);

    // Destructor
    ~Sebastian();

    void doWork();

private:
    static int m_objetsCounter;
    static bool m_workDone;

    int m_number;
    char m_character;

    int *m_pointer;
};

#endif // SEBASTIAN_H
