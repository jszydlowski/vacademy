#include "sebastian.h"

int main()
{
    Sebastian s1;
    Sebastian s2(2, 'c');
    Sebastian s3 = s1;

    s1.doWork();
    s2.doWork();
    s3.doWork();
    return 0;
}
