#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>


class Sudoku
{
public:
    Sudoku();

    int board[9][9] {{0}};

    void gameStart();

    void readFile();
    void printBoard();
    void checkResults();
    bool typeNumber();

    int m_number;
    int m_x;
    int m_y;
};



#endif // SUDOKU_H
