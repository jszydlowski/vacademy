#include "sudoku.h"
#include <qdebug>

Sudoku::Sudoku()
{

}

void Sudoku::gameStart()
{
    while(true)
    {
        typeNumber();

        printBoard();
        checkResults();
        // printBoard();
    }
}

void Sudoku::readFile()
{

}

void Sudoku::printBoard()
{
    for(int i =0; i<9 ;i++)
    {
        for(int j =0; j<9 ;j++)
        {
            std::cout << " " << board[j][i] << " ";
        }
        std::cout << std::endl;
    }
}

void Sudoku::checkResults()
{

    //vertical
    for(int i = 0; i < 9; i++)
    {
        bool errorTable[9] {false};

        for(int j = 0; j < 9; j++)
        {
            if(errorTable[(board[i][j])] == true)
            {
                std::cout<<" Error Repeated number " << std::endl;
            }
            else
            {
                if(board[i][j] != 0)
                {
                    errorTable[(board[i][j])] = true;
                }
            }
        }
    }
    //gorizontal
    for(int i = 0; i < 9; i++)
    {
        bool errorTable[9] {false};

        for(int j = 0; j < 9; j++)
        {
            if(errorTable[(board[j][i])] == true)
            {
                std::cout<<" Error Repeated number " << std::endl;
            }
            else
            {
                if(board[j][i] != 0)
                {
                    errorTable[(board[j][i])] = true;
                }
            }
        }
    }
    //areo
    int boardX = m_x / 3;
    int boardY = m_y / 3;

    bool errorTable[9] {false};

    for(int i = (boardX* 3); i < ((boardX* 3)+3) ;i++)
    {
        for(int j = (boardY* 3); j < ((boardY* 3)+3) ;j++)
        {
            if(errorTable[(board[i][j])] == true)
            {
                std::cout << "Error Repeated number" << std::endl;
            }
            if(board[j][i] != 0)
            {
                errorTable[(board[j][i])] = true;
            }
        }
    }

}

bool Sudoku::typeNumber()
{
    std::cout << "give me a number: ";
    std::cin >> m_number;
    if(m_number > 9 || m_number < 1)
    {
        std::cout << "error" <<std::endl;
        return 0;
    }


    std::cout << std::endl << "specify coordinates: " << std::endl;
    std::cin >> m_x;

    std::cin >> m_y;

    if(m_x > 8 || m_x < 0 || m_y > 8 || m_y < 0)
    {
        std::cout << "error" <<std::endl;
        return 0;
    }

    board[m_x][m_y] = m_number;

    return 1;

}
