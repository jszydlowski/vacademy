#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cctype>
#include <conio.h>

const int BOARD_SIZE = 9;
int board[BOARD_SIZE][BOARD_SIZE] = {};

void initBoard()
{
    std::ifstream file("board");

    if (file.is_open())
    {
        char character;
        int value;
        int row = 0, col = 0;

        while (file >> character)
        {
            value = atoi(&character);

            board[row][col++] = value;

            if (col >= BOARD_SIZE)
            {
                row++;
                col = 0;
            }
        }
    }
    else
    {
        std::cout << "File can not be open." << std::endl;
    }
}

void printBoard()
{
    system("cls");
    for (int row = 0; row < BOARD_SIZE; ++row)
    {
        if (row == 0)
        {
            std::cout << "  012 345 678" << std::endl;
            std::cout << " |-----------|" << std::endl;
        }
        for (int col = 0; col < BOARD_SIZE; ++col)
        {
            if (col == 0)
                std::cout << row << "|";

            if (board[row][col] == 0)
                std::cout << " ";
            else
                std::cout << board[row][col];

            if ((col+1) % 3 == 0)
            {
                std::cout << "|";
            }
        }

        if ((row+1) % 3 == 0)
            std::cout << "\n |-----------|";

        std::cout << std::endl;
    }
}

bool validateInput(const int row, const int col, const int value)
{
    bool validInput = true;
    std::cout << std::endl;

    if (std::cin.fail())
    {
        std::cout << "Enter only digits!" << std::endl;
        validInput = false;
    }

    if (row < 0 || row > BOARD_SIZE-1)
    {
        std::cout << "Enter valid row number!" << std::endl;
        validInput = false;
    }
    if (col < 0 || col > BOARD_SIZE-1)
    {
        std::cout << "Enter valid column number!" << std::endl;
        validInput = false;
    }

    const int MIN_VALUE = 1;
    const int MAX_VALUE = 9;

    if (value < MIN_VALUE || value > MAX_VALUE)
    {
        std::cout << "Enter valid value!" << std::endl;
        validInput = false;
    }

    if (!validInput)
    {
        std::cout << "Press any key to continue...";
        getch();
    }

    return validInput;

}

bool applyAnswer(const int row, const int col, const int value)
{
    // check duplications in rows and columns
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        if (board[row][i] == value || board[i][col] == value)
            return false;
    }

    // check duplication in 3x3 square
    int squareRowStart;
    int squareRowEnd;
    if (row < 3)
    {
        squareRowStart = 0;
        squareRowEnd = 2;
    }
    else if (row < 6)
    {
        squareRowStart = 3;
        squareRowEnd = 5;
    }
    else
    {
        squareRowStart = 6;
        squareRowEnd = 8;
    }

    int squareColumnStart;
    int squareColumnEnd;
    if (col < 3)
    {
        squareColumnStart = 0;
        squareColumnEnd = 2;
    }
    else if (col < 6)
    {
        squareColumnStart = 3;
        squareColumnEnd = 5;
    }
    else
    {
        squareColumnStart = 6;
        squareColumnEnd = 8;
    }

    for (int i = squareRowStart; i < squareRowEnd; ++i)
    {
        for (int j = squareColumnStart; j < squareColumnEnd; ++j)
        {
            if (board[i][j] == value)
                return false;
        }
    }

    board[row][col] = value;

    return true;
}

bool isEnd()
{
    for (int row = 0; row < BOARD_SIZE; ++row)
    {
        for (int col = 0; col < BOARD_SIZE; ++col)
        {
            if (board[row][col] == 0)
                return false;
        }
    }

    return true;
}

bool isInputFail()
{
    if (std::cin.fail())
    {
        std::cout << "\nOnly digits, please!" << std::endl;
        std::cin.clear();
        std::cin.sync();
        std::cout << "Press any key to continue...";
        getch();
        return true;
    }
    return false;
}

int main()
{
    initBoard();

    bool end = false;

    while (!end)
    {
        printBoard();
        std::cout << std::endl;

        int row;
        std::cout << "Enter row number: ";
        std::cin >> row;
        if (isInputFail())
            continue;

        int col;
        std::cout << "Enter column number: ";
        std::cin >> col;
        if (isInputFail())
            continue;

        int value;
        std::cout << "Enter value: ";
        std::cin >> value;
        if (isInputFail())
            continue;

        if (!validateInput(row, col, value))
            continue;

        if (!applyAnswer(row, col, value))
        {
            std::cout << "You can not have your number in this place due to sudoku rules!" << std::endl;
            std::cout << "Press any key to continue...";
            getch();
            continue;
        }

        if (isEnd())
            end = true;
    }

    printBoard();
    std::cout << "\n\nCongratulations! You have completed this sudoku puzzle!";


    std::cout << std::endl;
    return 0;
}
