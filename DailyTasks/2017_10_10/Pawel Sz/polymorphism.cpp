#include <iostream>

static const int BLUE = 1;
static const int RED  = 2;

class Shape
{
public:
	virtual void draw() = 0;
	virtual void fill(int) = 0;
	virtual ~Shape()
	{
		std::cout << "Virtual destructor! " << std::endl;
	}
};

class Rectangle: public Shape
{
private:
	int posX, posY, height, width, size;
	bool flag;

public:

	/*
	 * default constructor - set all protected variables to zero
	 */
	Rectangle():
		posX(0),
		posY(0),
		height(0),
		width(0),
		size(0),
		flag(0)

	{
		;
	}

    /*
     * constructor with parameters. Set correct values of shape
     * INPUT:
     * int posXin: position on the X axis
     * int posYin: position on the Y axis
     * int heightIn: height of shape
     * int widthIn: width of shape
     * bool flagIn: flag for i.e. drawing on top
     */
	Rectangle(int posXin, int posYin, int heightIn, int widthIn, bool flagIn):
		posX(posXin),
		posY(posYin),
		height(heightIn),
		width(widthIn),
		flag(flagIn)
	{
		;
	}

	/*
	 * draw method: will draw rectangle on the screen
	 */
	virtual void draw()
	{
		size = posX * posY;
		std::cout << "Rectangle draw: " << size << std::endl;
	}

	/*
	 * fill method: will fill with given color already created shape. Method draw should be called as first, or should be invoked by fill method
	 */
	virtual void fill(int colorIn)
	{
		std::cout << "Rectangle fill: " << size << " filled" << std::endl;
	}

	~Rectangle()
	{
		std::cout << "Rectangle destructor! " << std::endl;
	}

};

/////////////// CIRCLE CLASS //////////////////////////////////////////

class Circle: public Shape
{
private:
private:
	int posX, posY, height, size;
	bool flag;
	const float PI = 3.1415;
public:

	/*
	 * default constructor - set all protected variables to zero
	 */
	Circle():
		posX(0),
		posY(0),
		height(0),
		flag(0)
	{
		;
	}

    /*
     * constructor with parameters. Set correct values of shape
     * INPUT:
     * int posXin: position on the X axis
     * int posYin: position on the Y axis
     * int heightIn: height of shape
     * int widthIn: width of shape
     * bool flagIn: flag for i.e. drawing on top
     */
	Circle(int posXin, int posYin, int heightIn, bool flagIn):
		posX(posXin),
		posY(posYin),
		height(heightIn),
		flag(flagIn)
	{
		;
	}

	/*
	 * draw method: will draw circle on the screen
	 */
	virtual void draw()
	{
		size = PI * height * height;
		std::cout << "Circle draw: " << size << std::endl;
	}

	/*
	 * fill method: will fill with given color already created shape. Method draw should be called as first, or should be invoked by fill method
	 */
	virtual void fill(int color)
	{
		std::cout << "Circle fill: " << size << " filled" << std::endl;
	}

	~Circle()
	{
			std::cout << "Circle destructor! " << std::endl;
	}

};

int main()
{

	/*
	 * Rectangle object: int posXin, int posYin, int heightIn, int widthIn, bool flagIn
	 */
	Rectangle rect(10, 10, 5, 7, 0);

	/*
	 * circle object: int posXin, int posYin, int heightIn, bool flagIn
	 */
	Circle circ(10, 10, 5, 1);

	Shape *ptr;

	//Circle
	ptr = &circ;
	ptr->draw();
	ptr->fill(RED);

	//Rectangle
	ptr = &rect;
	ptr->draw();
	ptr->fill(BLUE);

}
