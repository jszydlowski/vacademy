#include <iostream>
#include <typeinfo>

template <typename T>
void ptintIt(T firstParm)
{
	std::cout << firstParm << std::endl;
	std::cout << typeid(firstParm).name();
};


int main()
{
	std::string parm= "test";
	ptintIt(parm);

	std::cout << std::endl;

	auto parm2= 2;
	ptintIt(parm2);
}
