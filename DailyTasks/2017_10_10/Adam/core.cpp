#include "core.h"

Core::Core(bool visibility, double x, double y)
    : m_visibility(visibility), m_x(x) , m_y(y)
{

}

void Core::printName()
{
    std::cout << "base"<< std::endl;
}

void Core::setVisibility(bool visibility)
{
    m_visibility = visibility;
}

void Core::setX(double x)
{
    m_x = x;
}

void Core::setY(double y)
{
    m_y = y;
}
