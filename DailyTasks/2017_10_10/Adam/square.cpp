#include "square.h"

Square::Square(double diagonal, bool visibility, double x, double y)
    :Core(visibility, x, y), m_diagonal(diagonal)
{

}

void Square::printName()
{
    std::cout << "i am Square!" << std::endl;
}

void Square::setDiagonal(double diagonal)
{
    m_diagonal = diagonal;
}
