#ifndef SQUARE_H
#define SQUARE_H
#include <core.h>

class Square: public Core
{
public:
    Square(double diagonal, bool visibility, double x, double y);

    // Core interface

    void printName();
    void setDiagonal(double diagonal);

private:
    double m_diagonal;
};

#endif // SQUARE_H
