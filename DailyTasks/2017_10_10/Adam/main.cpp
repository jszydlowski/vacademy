#include <iostream>
#include <core.h>
#include <circle.h>
#include <square.h>


int main()
{
    Core point(1 , 4 , 8);
    Circle wheel(2, 4, 5, 7);
    Square cube(2, 4, 5, 7);

    point.printName();
    wheel.printName();
    cube.printName();

    return 0;
}
