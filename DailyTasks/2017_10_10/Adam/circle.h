#ifndef CIRCLE_H
#define CIRCLE_H
#include <core.h>


class Circle: public Core
{
public:

    Circle(double r ,bool visibility, double x, double y);

    // Core interface

    void printName();

    void setR(double r);

private:
    double m_r;
};

#endif // CIRCLE_H
