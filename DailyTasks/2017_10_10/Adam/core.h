#ifndef CORE_H
#define CORE_H

#include <iostream>

class Core
{
public:

    Core(bool visibility , double x , double y);

    virtual void printName();



    void setVisibility(bool visibility);
    void setX(double x);
    void setY(double y);

protected:

    bool m_visibility;
    double m_x;
    double m_y;
};

#endif // CORE_H
