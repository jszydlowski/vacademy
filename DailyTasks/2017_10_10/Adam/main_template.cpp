#include <iostream>
#include <typeinfo>

using std::cout;

template <class object>
void templatePrint(object toPrint);
int main()
{
    int a = 6;
    double b = 5.6;
    std::string c = "test";

    templatePrint(a);
    templatePrint(b);
    templatePrint(c);

    return 0;
}
template <class object>
void templatePrint(object toPrint)
{

    cout << typeid(toPrint).name()<<"   " << toPrint << std::endl;
}
