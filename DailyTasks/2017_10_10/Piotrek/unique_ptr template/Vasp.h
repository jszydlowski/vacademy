#ifndef Vasp_h
#define Vasp_h

/**
 * @brief The Vasp class -> Class that simulates unique_ptr.
 */
template <typename T>
class Vasp
{
public:
    /**
     * @brief Vasp -> constructor that creates instance
     * of vasp and sets it as nullptr.
     */
    Vasp() :
        m_dataRawPtr(nullptr)
    {}
    /**
     * @brief Vasp -> constructor that creates instance
     * of vasp and sets given argument to m_dataRawPtr.
     * @param data
     */
    Vasp(T *data) :
         m_dataRawPtr(data)
    {}

    Vasp(const T&) = delete;

    ~Vasp()
    {
        releasePtr();
    }
    
    /**
     * @brief operator * -> operator * is now used to
     * return value of m_dataRowPtr;
     * @return -> value of m_dataRowPtr;
     */
    T& operator* ()
    {
        return *m_dataRawPtr;
    }
    /**
     * @brief operator = -> operator "=" will remove actual object of
     * m_dataRowPtr and sets new object for him.
     * @param data -> instance of Data.
     */
    void operator= (T *data)
    {
        delete m_dataRawPtr;

        m_dataRawPtr = data;
    }

    void operator= (const T&) = delete;

    /**
     * @brief operator -> operator "->" allows us to directly call
     * an attribute of Data.
     * @return
     */
    T* operator-> ()
    {
        if (m_dataRawPtr == nullptr)
        {
            return nullptr;
        }

        return m_dataRawPtr;
    }
    
private:
    /**
     * @brief m_dataRawPtr -> normal pointer for Data objects.
     */
//    template <class T>
    T *m_dataRawPtr;
    /**
     * @brief releasePtr -> method that releases m_dataRowPtr and
     * remove reference from reference counter.
     */
    void releasePtr()
    {
        delete m_dataRawPtr;
        m_dataRawPtr = nullptr;
    }
};

#endif /* Vasp_h */
