#include <iostream>
#include <Vasp.h>

struct Piotrek
{
    std::string m_name {"Piotrek"};
    std::string m_surname {"Gawronski"};
};

int main()
{
    Vasp<Piotrek> myVasp(new Piotrek);

    std::cout << myVasp->m_name << std::endl;
    std::cout << myVasp->m_surname << std::endl;

    return 0;
}
