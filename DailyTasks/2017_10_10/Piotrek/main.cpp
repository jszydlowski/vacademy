#include <iostream>
#include <typeinfo>

template <typename T>
void printSomething(const T &something)
{
    std::cout << something << std::endl;
    std::cout << "Variable type: " << typeid(something).name() << std::endl << std::endl;
}

int main(int argc, const char * argv[])
{
    int i = 10;
    std::string text = "siemka";
    char character = 'z';

    printSomething(i);
    printSomething(text);
    printSomething(character);

    return 0;
}
