#include <iostream>

#include "shapefactory.h"
#include "shape.h"
#include "circle.h"
#include "square.h"
#include "triangle.h"

int main()
{
    std::cout << "Type: \tsquare\n\tcircle\n\ttriangle\nThen enter side value "
                 "to get field\n\n" << std::endl;

    std::string wantedObject = "";
    std::cin >> wantedObject;

    ShapeFactory factory(wantedObject);

    Shape *shape_ptr = factory.returnInstance();

    if (shape_ptr == nullptr)
    {
        std::cout << "Wrong object name" << std::endl;
    }
    else
    {

    std::cout << "Enter side value" << std::endl;
    int side = 0;
    std::cin >> side;

    shape_ptr->setSide(side);
    shape_ptr->calculateField();
    shape_ptr->drawFigure();

    delete shape_ptr;
    }

    return 0;
}
