#include "shape.h"

#include <iostream>

void Shape::drawFigure()
{
    std::cout << "Drawing" << std::endl;
}

void Shape::setSide(const int &value)
{
    m_side = value;
}
