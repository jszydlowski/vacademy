#ifndef STRINGTOENUM_H
#define STRINGTOENUM_H

enum ObjectName
{
    None,
    CircleObject,
    SquareObject,
    TriangleObject
};

#endif // STRINGTOENUM_H
