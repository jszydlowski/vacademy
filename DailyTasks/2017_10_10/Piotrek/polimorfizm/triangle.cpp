#include "triangle.h"

#include <iostream>

Triangle::Triangle()
{

}

Triangle::~Triangle()
{

}

void Triangle::calculateField()
{
    const int TRIANGLE_HIGH = 2;
    int triangleField = m_side * TRIANGLE_HIGH / 2;

    std::cout << "Triangle field: " << triangleField << std::endl;
}
