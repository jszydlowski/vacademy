#ifndef SHAPE_H
#define SHAPE_H


class Shape
{
public:
    virtual void drawFigure();
    virtual void calculateField() = 0;
    virtual void setSide(const int &value);
    virtual ~Shape() {}

protected:
    int m_side;
};

#endif // SHAPE_H
