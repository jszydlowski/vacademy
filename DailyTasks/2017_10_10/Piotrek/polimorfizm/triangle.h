#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "shape.h"

class Triangle : public Shape
{
public:
    Triangle();
    ~Triangle();

    void calculateField();
};

#endif // TRIANGLE_H
