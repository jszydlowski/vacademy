#ifndef SHAPEFACTORY_H
#define SHAPEFACTORY_H

#include <iostream>
#include <map>

#include "stringtoenum.h"
#include "shape.h"

class Circle;
class Square;
class Triangle;

class ShapeFactory
{
public:
    ShapeFactory(const std::string &input);

    Shape* returnInstance();

private:
    std::string m_inputFromUser;
    std::map<std::string, ObjectName> m_values;
};

#endif // SHAPEFACTORY_H
