#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.h"

class Circle : public Shape
{
public:
    Circle();
    ~Circle();

    void calculateField();
};

#endif // CIRCLE_H
