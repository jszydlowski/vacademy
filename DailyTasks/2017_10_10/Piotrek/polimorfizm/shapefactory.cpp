#include "shapefactory.h"

#include "circle.h"
#include "square.h"
#include "triangle.h"

ShapeFactory::ShapeFactory(const std::string &input) :
    m_inputFromUser(input)
{
    m_values["circle"] = ObjectName::CircleObject;
    m_values["square"] = ObjectName::SquareObject;
    m_values["triangle"] = ObjectName::TriangleObject;
}

Shape *ShapeFactory::returnInstance()
{
    ObjectName wantedObject {m_values[m_inputFromUser]};

    switch (wantedObject)
    {
        case ObjectName::CircleObject:
            return new Circle();
            break;

        case ObjectName::SquareObject:
            return new Square();
            break;

        case ObjectName::TriangleObject:
            return new Triangle();
            break;

        default:
            return nullptr;
            break;
    }
}
