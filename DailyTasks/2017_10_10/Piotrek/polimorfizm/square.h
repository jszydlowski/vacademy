#ifndef SQUARE_H
#define SQUARE_H

#include "shape.h"

class Square : public Shape
{
public:
    Square();
    ~Square();

    void calculateField();
};

#endif // SQUARE_H
