#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "shape.h"

class Rectangle : public Shape
{
    double a;

public:

    Rectangle(double sideConstruct);

    void print();
    double square();
};

#endif // RECTANGLE_H
