#include <iostream>
#include "shape.h"
#include "rectangle.h"
#include "triangle.h"




int main(int argc, char *argv[])
{
     Shape shape;
     Rectangle rectangle(5);
     Triangle triangle(4, 5.5);

     shape.print();
     shape.square();

     rectangle.print();
     rectangle.square();

     triangle.print();
     triangle.square();

     std::cout<<"\nusing pointers: \n"<<std::endl;

     Shape *shapePtr = 0;

     shapePtr = &shape;
     shapePtr->print();

     shapePtr = &rectangle;
     shapePtr->print();

     shapePtr = &triangle;
     shapePtr->print();










}
