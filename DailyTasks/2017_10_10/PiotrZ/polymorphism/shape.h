#ifndef SHAPE_H
#define SHAPE_H
#include <iostream>

class Shape
{
public:

    virtual void print();
    virtual double square();
};


#endif // SHAPE_H
