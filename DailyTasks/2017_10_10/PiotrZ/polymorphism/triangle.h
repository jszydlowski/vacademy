#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "shape.h"

class Triangle : public Shape
{
    int base;
    double height;

public:

    Triangle(int baseConstruct, double heightConstruct);

    void print();
    double square();
};

#endif // TRIANGLE_H
