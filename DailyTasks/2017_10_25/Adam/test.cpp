#include <tests.h>

bool test1()
{
    char testSring[] = "AAAb";
    int expectedValue = 2;

    if(min_max(testSring) == expectedValue ){
        return true;
    }
    else{
        return false;
    }

}

bool test2()
{
    char testSring[] = "AAAab";
    int expectedValue = 2;

    if(min_max(testSring) == expectedValue ){
        return true;
    }
    else{
        return false;
    }
}

bool test3()
{
    char testSring[] = "AAAa";
    int expectedValue = 2;

    if(min_max(testSring) == expectedValue ){
        return true;
    }
    else{
        return false;
    }
}

bool test4()
{
    char testSring[] = "AAaa";
    int expectedValue = 0;

    if(min_max(testSring) == expectedValue ){
        return true;
    }
    else{
        return false;
    }
}
bool test5()
{
    char testSring[] = "abcB";
    int expectedValue = 0;

    if(min_max(testSring) == expectedValue ){
        return true;
    }
    else{
        return false;
    }
}
