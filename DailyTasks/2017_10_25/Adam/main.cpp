#include <iostream>
#include <tests.h>
//using namespace std;

//void min_max(char* str);


int main()
{
    std::cout << test1() << std::endl;
    std::cout << test2() << std::endl;
    std::cout << test3() << std::endl;
    std::cout << test4() << std::endl;
    std::cout << test5() << std::endl;



    return 0;
}
int min_max(char* str)
{
    int i = 0;
    int ascii[256] = {0};

    while(str[i] != 0)
    {
        ascii[(str[i])] += 1;

        i++;
    }


    int max = 0;
    int min = 10;
    for(int i = 0; i < 256; i++)
    {
        if(max < ascii[i])
        {
            max = ascii[i];
        }
        if(min > ascii[i] && ascii[i] != 0)
        {
            min = ascii[i];
        }
    }

    return (max - min);
}
