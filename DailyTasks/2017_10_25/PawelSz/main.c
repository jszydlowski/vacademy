/*
 * output: number of max minus min
 * znak wystepuje najwiecej razy, odjecie ilosci znaku, ktory wystepuje najmniej razy. a != A
 */

//TODO:
/* make const static
 * make functions return info
 * check null pointers
 * */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "main.h"

static const int OK = 0;
static const int NOK = 1;
static const int NULL_POINTER = 2;


int main()
{
    char str[] = "aBcDkajjjsd";
    MinMax(str);

    {
        // TEST CASE 1

        char str[] = "aBcDkajjjsd";
        int expected = 2;
        assertP(str, expected);
    }

    {
        // TEST CASE 2

        char str[] = "bbGGggIhsSa";
        int expected = 1;
        assertP(str, expected);
    }

    {
        // TEST CASE 3

        char str[] = "aa0";
        int expected = 1;
        assertP(str, expected);
    }

    {
        // TEST CASE 4

        char str[] = "aaaHHoPPpppp";
        int expected = 3;
        assertP(str, expected);
    }

    {
        // TEST CASE 5

        char str[] = "oPPpppp";
        int expected = 3;
        assertP(str, expected);
    }

    {
        // TEST CASE 6

        char str[] = "";
        int expected = 0;
        assertP(str, expected);
    }

    {
        // TEST CASE 7
        // RANDOM CHARACTERS, TEST SHOULD FAIL

        char str[] = "oPPA:SFJpppp";
        int expected = 10;
        assertP(str, expected);
    }



    return 0;
}

int MinMax(char* str)
{
    if(str == 0)
    {
        return NULL_POINTER;
    }

    else
    {
        int string_length = strlen(str);

        if(!string_length)
        {
            return 0;
        }

        SortArr(str, string_length);
        char values[string_length];
        memset(values, 0, string_length); // set all values of an array to 0

        int count_chars = 1;
        int count_groups = 0;

        for(int i = 0; i < string_length - 1; i++)
        {
            if(str[i] == str [i + 1])
            {
                ++count_chars;
            }

            else
            {
                values[count_groups] = count_chars; // save number of counted chars to the array
                count_chars = 1; // reset chars counter
                ++count_groups; // increment for next array position id another group will occur
            }
        }

        if(str[string_length - 2] == str [string_length - 1]) // if last character in an array is same as previous
        {
            values[count_groups] = count_chars; // save number of counted chars to the array
            count_chars = 1; // reset chars counter
            ++count_groups; // increment for next array position id another group will occur
        }

        SortArr(values, count_groups);
        int result = values[count_groups - 1] - values[0];

        return result;
    }

    return NOK;

} // MinMax func

uint8_t SortArr(char* str, uint8_t string_length)
{
    if(str == 0)
    {
        return NULL_POINTER;
    }

    else
    {
        // sort
        for(int i = 0; i < string_length; i++)
        {
            for(int j = 0; j < string_length; j++)
            {
                if(str[i] < str[j])
                {
                    str[i] ^= str[j];
                    str[j] ^= str[i];
                    str[i] ^= str[j];
                }
            }
        }
        return OK;
    }

    return NOK;

} // SortAtt func
