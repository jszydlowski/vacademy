#ifndef TEST_FRAMEWORK_H
#define TEST_FRAMEWORK_H

#include <stdint.h>

uint8_t assertP(const char * strIn, uint16_t expected);
uint8_t result;

#endif // TEST_FRAMEWORK_H
