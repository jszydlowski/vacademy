#ifndef MAIN_H
#define MAIN_H

#include "test_framework.h"

int MinMax(char* str);
uint8_t SortArr(char* str, uint8_t string_length);

#endif // MAIN_H
