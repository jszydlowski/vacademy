#include "test_framework.h"
#include "main.h"

uint8_t assertP(const char * strIn, uint16_t expected)
{
    static int test_counter;
    result = 0;
    ++test_counter;
    result = MinMax(strIn);

    if(result == expected)
    {
        printf("test %d passed\n", test_counter);
    }

    else
    {
        printf("\ntest %d fails\n", test_counter);
        printf("returned: %d\nexpected: %d\n", result, expected);
    }
}
