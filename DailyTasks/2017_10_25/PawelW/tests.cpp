#include <iostream>
#include <cstdlib>
#include <random>
#include <functional>
#include <climits>
#include "min_max.h"
#include "tests.h"

using namespace std;

/**
 * @brief EMPTY_STIRNG_TEST -> 
 * function min_max should return value 0 for an empty char*
 */

void EMPTY_STRING_TEST() {
    bool result;
    try {
        MinMax m;
        char* c = new char[1] {
        };

        if (m.min_max(c) == 0) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "EMPTY_STRING_TEST passed" << endl;
        } else {
            cout << "EMPTY_STRING_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "EMPTY_STRING_TEST failed to launch" << endl;
    }
}

/**
 * @brief UPPERCASE_LOWERCASE_DISTINCTION_TEST -> 
 * function min_max should be able to distinguish between upper and lower case letters as different chars
 */

void UPPERCASE_LOWERCASE_DISTINCTION_TEST() {
    bool result;
    try {
        MinMax m;
        char* c = new char[6] {
            'A', 'A', 'B', 'a', 'b'
        };

        if (m.min_max(c) == 1) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "UPPERCASE_LOWERCASE_DISTINCTION_TEST passed" << endl;
        } else {
            cout << "UPPERCASE_LOWERCASE_DISTINCTION_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "UPPERCASE_LOWERCASE_DISTINCTION_TEST failed to launch" << endl;
    }
}

/**
 * @brief RANDOM_CHAR_TABLE_TEST -> 
 * function min_max should be able to handle very big char* without failure
 */

void RANDOM_CHAR_TABLE_TEST() {

    int SIZE_OF_TABLE = 1000000;

    try {

        char* c = new char[SIZE_OF_TABLE];
        MinMax m;

        random_device rd;
        mt19937 gen(rd());
        uniform_int_distribution<char> dis(0, UCHAR_MAX);
        auto res = bind(dis, gen);

        for (int i = 0; i < SIZE_OF_TABLE; i++) {
            c[i] = res();
        }

        int result = m.min_max(c);
        delete c;

        cout << "RANDOM_CHAR_TABLE_TEST succeeded to work for: " << SIZE_OF_TABLE
                << " elements" << endl;

    } catch (...) {
        cout << "RANDOM_CHAR_TABLE_TEST failed to launch for: " << SIZE_OF_TABLE
                << " elements" << endl;
    }
}

/**
 * @brief SIMILAR_DISTRIBUTION_TEST -> 
 * function min_max should return 0 if every character in char* is different
 */

void SIMILAR_DISTRIBUTION_TEST() {
    bool result;
    try {
        char* c = new char[11] {
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j'
        };
        MinMax m;
        if (m.min_max(c) == 0) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "SIMILAR_DISTRIBUTION_TEST passed" << endl;
        } else {
            cout << "SIMILAR_DISTRIBUTION_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "SIMILAR_DISTRIBUTION_TEST failed to launch" << endl;
    }

}

/**
 * @brief EMPTY_CHAR_INSIDE_TEST -> 
 * function min_max should ignore empty characters. 
 */
/*
void EMPTY_CHAR_INSIDE_TEST() {
    bool result;
    try {
        char* c = new char[11] {
            'a', ' ', 'a', 'a', ' ',
            'f', 'g', ' ', 'i', 'j'
        };
        MinMax m;
        if (m.min_max(c) == 2) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "EMPTY_CHAR_INSIDE_TEST passed" << endl;
        } else {
            cout << "EMPTY_CHAR_INSIDE_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "EMPTY_CHAR_INSIDE_TEST failed to launch" << endl;
    }
}
*/
/**
 * @brief ONE_CHAR_TEST -> 
 * function min_max should handle situation when only one character is passed to the function. 
 */

void ONE_CHAR_TEST(){
    bool result;
    try {
        char* c = new char[2] {
            'a'
        };
        MinMax m;
        if (m.min_max(c) == 0) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "ONE_CHAR_TEST passed" << endl;
        } else {
            cout << "ONE_CHAR_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "ONE_CHAR_TEST failed to launch" << endl;
    }
}

/**
 * @brief SPECIAL_CHAR_TEST -> 
 * function min_max should ignore special characters like e.g. "\n". 
 */

void SPECIAL_CHAR_TEST(){
    bool result;
    try {
        char* c = new char[6] {
            '\n', '\t', 'a', 'a', 'b'
        };
        MinMax m;
        if (m.min_max(c) == 1) {
            result = true;
        } else
            result = false;

        if (result) {
            cout << "SPECIAL_CHAR_TEST passed" << endl;
        } else {
            cout << "SPECIAL_CHAR_TEST failed" << endl;
        }
        delete c;
    } catch (...) {
        cout << "SPECIAL_CHAR_TEST failed to launch" << endl;
    }
}






