#include <cstdlib>
#include <iostream>
#include "tests.h"

using namespace std;


int main(int argc, char** argv) {

    EMPTY_STRING_TEST();
    UPPERCASE_LOWERCASE_DISTINCTION_TEST();
    RANDOM_CHAR_TABLE_TEST();
    SIMILAR_DISTRIBUTION_TEST();
   // EMPTY_CHAR_INSIDE_TEST();
    ONE_CHAR_TEST();
    SPECIAL_CHAR_TEST();
      
    return 0;
}

