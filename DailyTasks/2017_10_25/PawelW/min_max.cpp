/**
 * @brief min_max()
 * @param char*
 * @return int
 * 
 * function min_max returns a number representing maximum quantity of a specific character 
 * substracted by minimum quantity of a specific character. 
 */


//CharInString(char* needle, string* haystack)
#include <cstdlib>
#include <algorithm>
#include <map>
#include "min_max.h"

using namespace std;

int MinMax::min_max(char* c) {
    int startCount = 0;
    int sizeOfChar = 0;
    int MaxValue = 0;
    int MinValue;
    int qty;
    
    //Calcualting length of char*
    for (int i = 0; c[i] != '\0'; i++) {
        sizeOfChar++;
    }
    
    map<unsigned char, int> charCount;

    for (int i = 0; c[i] != '\0'; i++) {
        qty = charCount[c[i]]++;
        
        charCount.insert(pair<unsigned char,int>(c[i],qty));
    }
    

    
    MinValue = charCount[c[0]];
    
    for (int i = 0; i < sizeOfChar; i++) {
        if (charCount[c[i]] > MaxValue)
            MaxValue = charCount[c[i]];
        if(charCount[c[i]] < MinValue)
            MinValue = charCount[c[i]];
    }

    return MaxValue - MinValue;
}
