#ifndef FUNCTION_H
#define FUNCTION_H

#include <map>
#include <cstring>
#include <iostream>
#include <limits>

int min_max (char *str)
{
    if (str == nullptr)
    {
        return 0;
    }

    int charactersCount = strlen(str);
    std::map<char, int> inputWord;

    for (int i = 0; i < charactersCount; ++i)
    {
        inputWord[str[i]]++;
    }

    int maxValue = [&inputWord]()->int{
            int currentMaxValue = INT_MIN;

            for (auto value : inputWord)
            {
                if (value.second > currentMaxValue)
                {
                    currentMaxValue = value.second;
                }
            };

            return currentMaxValue;
            }();

    int minValue = [&inputWord]()->int{
            int currentMinValue = INT_MAX;

            for (auto value : inputWord)
            {
                if (value.second < currentMinValue)
                {
                    currentMinValue = value.second;
                }
            };

            return currentMinValue;
            ;}();

    return maxValue - minValue;
}

#endif // FUNCTION_H
