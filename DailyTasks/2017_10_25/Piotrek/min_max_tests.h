#ifndef MIN_MAX_TESTS_H
#define MIN_MAX_TESTS_H

class Min_Max_Tests
{
public:
    Min_Max_Tests();

    bool test(char *str, const int &expectedResult);
    void runTests();
};

#endif // MIN_MAX_TESTS_H
