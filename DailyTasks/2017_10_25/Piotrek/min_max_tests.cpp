#include "min_max_tests.h"

#include "minMax.h"

#include <iostream>


Min_Max_Tests::Min_Max_Tests()
{

}

bool Min_Max_Tests::test(char *str, const int &expectedResult)
{
    int result = min_max(str);

    if (result == expectedResult)
    {
        return true;
    }

    return false;
}

void Min_Max_Tests::runTests()
{
    if (test("AAAbb", 1))
    {
        std::cout << "Normal case test PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Normal case test FAIL" << std::endl << std::endl;
    }

    if (test("##AAA", 1))
    {
        std::cout << "Special characters PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Special characters FAIL" << std::endl << std::endl;
    }

    if (test("BBBAAA", 0))
    {
        std::cout << "Min max equal PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Min max equal FAIL" << std::endl << std::endl;
    }

    if (test("12AAA", 2))
    {
        std::cout << "Case with numbers PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Case with numbers FAIL" << std::endl << std::endl;
    }

    if (test("aaaaa", 0))
    {
        std::cout << "Case with only one character PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Case with only one character FAIL" << std::endl << std::endl;
    }

    if (test(nullptr, 0))
    {
        std::cout << "Test if nullptr PASS" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Test if nullptr FAIL" << std::endl << std::endl;
    }
}
