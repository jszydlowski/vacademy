#include <iostream>
#include <cstring>
#include <algorithm>

void test(const int& actual, const int& expected)
{
    if (actual == expected)
        std::cout << "PASSED" << std::endl;
    else
        std::cout << "FAILED!!!" << std::endl;
}

int minMax(const char* str)
{
    if (!str)
        return -1;

    int length = strlen(str);

    if (length == 0 || length == 1)
        return 0;

    int min = length;
    int max = 1;

    for (int i = 0; i < length; ++i)
    {
        int count = std::count(str, str + length, str[i]);

        if (count > max)
            max = count;

        if (count < min)
            min = count;
    }

    int result = max - min;

    return result;
}

int main()
{
    test( minMax(nullptr) , -1 );
    test( minMax("") , 0 );
    test( minMax("a") , 0 );
    test( minMax("aaaaaaa") , 0 );
    test( minMax("abcdefg") , 0 );
    test( minMax("aaaabbbbcccc") , 0 );
    test( minMax("nullptr") , 1 );
    test( minMax("kajak") , 1 );
    test( minMax("a\n\n\n\na") , 2 );
    test( minMax("Nikt na świecie nie ma takich spodni jak ja.") , 6 ); // expected value shoud be 7


    return 0;
}
