#include <iostream>

/**
 * @brief power -> function that returns X^N
 * @param x -> base number
 * @param n -> exponent
 * @return -> X^N
 */
int power (int x, int n)
{
    if (x < 0 || n < 0) {
        throw "Bad values";
    }

    if (n == 0)
    {
        return 1;
    }
    else {
        return x = x * power(x, (n - 1));
    }
}

/**
 * @brief POWER_TEST -> function that tests if values in range
 * 0 - 8 are right for base number 2.
 */
void POWER_TEST ()
{
    const int BASE_POW_VALUE = 2;

    const int VALUES[] {1, 2, 4, 8, 16, 32, 64, 128, 256};
    const int VALUES_SIZE = sizeof(VALUES) / sizeof(int);

    bool testResult = true;

    for (int index = 0; index < VALUES_SIZE; ++index)
    {
        if (power(BASE_POW_VALUE, index) != VALUES[index])
        {
            testResult = false;
        }
    }

    if (testResult == false)
    {
        std::cout << "BAD" << std::endl;
    }
    else
    {
        std::cout << "GOOD" << std::endl;
    }
}

/**
 * @brief TEST_POWER_FOR_FIRST_NEGATIVE -> function that tests if
 * power function can return result for negative base number.
 */
void TEST_POWER_FOR_FIRST_NEGATIVE()
{
    try
    {
        power(2, -5);
        std::cout << "POSITIVE FIRST_NEGATIVE" << std::endl;
    }

    catch (...)
    {
        std::cout << "WRONG FIRST_NEGATIVE" << std::endl;
    }
}

/**
 * @brief TEST_POWER_FOR_SECOND_NEGATIVE -> function that tests if
 * power function can return result for negative second number.
 */
void TEST_POWER_FOR_SECOND_NEGATIVE()
{
    try
    {
        power(-1, 5);
        std::cout << "POSITIVE SECOND_NEGATIVE" << std::endl;
    }

    catch (...)
    {
        std::cout << "WRONG SECOND_NEGATIVE" << std::endl;
    }
}

/**
 * @brief TEST_BOTH_NEGATIVE -> function that tests if
 * power function can return result for both values.
 */
void TEST_BOTH_NEGATIVE()
{
    try
    {
        power(-1, -5);
        std::cout << "POSITIVE BOTH_NEGATIVE" << std::endl;
    }

    catch (...)
    {
        std::cout << "WRONG BOTH_NEGATIVE" << std::endl;
    }
}

int main()
{
    POWER_TEST();
    TEST_POWER_FOR_FIRST_NEGATIVE();
    TEST_POWER_FOR_SECOND_NEGATIVE();
    TEST_BOTH_NEGATIVE();

    return 0;
}
