#include<iostream>

using std::cout;
using std::endl;
using std::cin;

/**
 * @brief power - function that returns x^y
 * @param x     - base
 * @param y     - exponent
 * @return      - x^y
 * Entering signed exponent will result in function returning -1
 */

int power(int x, int y)
{
    if (y==0)
    {
        return 1;
    }
    if(x<0 || y<0)
    {
        return -1;
    }
        return x=x*power(x,(y-1));
}


int main(int argc, char *argv[])
{
//    int a, b;
//    cout<<" enter base: "<<endl;
//    cin>>a;
//    cout<<" enter exponent: "<<endl;
//    cin>>b;

    if(power(2,2) == 4)
    {
        cout<<"2^2 = 4";
    }
    if(power(2,3) == 8)
    {
        cout<<"2^3 = 8";
    }  if(power(3,4) == 243)
    {
        cout<<"3^4 = 243";
    }

}

