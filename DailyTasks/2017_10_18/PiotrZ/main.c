#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>

#define _planes 1
#define _compression 0
#define _xpixelpermeter 0x130B //2835 , 72 DPI
#define _ypixelpermeter 0x130B //2835 , 72 DPI
#define pixel 0x300 // our color

#pragma pack(push,1)

typedef struct{
    uint8_t signature[2];
    uint32_t filesize;
    uint32_t reserved;
    uint32_t fileoffset_to_pixelarray;
} fileheader;
typedef struct{
    uint32_t dibheadersize;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitsperpixel;
    uint32_t compression;
    uint32_t imagesize;
    uint32_t ypixelpermeter;
    uint32_t xpixelpermeter;
    uint32_t numcolorspallette;
    uint32_t mostimpcolor;
} bitmapinfoheader;
typedef struct {
    fileheader fileheader;
    bitmapinfoheader bitmapinfoheader;
} bitmap;

#pragma pack(pop)

uint32_t getPixelByteSize(int height, int width, int bitsPerPixel)
{
    return height*width*bitsPerPixel/8;
}

uint32_t getFileSize(uint32_t PixelByteSize)
{
    return PixelByteSize + sizeof(bitmap);
}

void setBitmapInfoHeader(bitmap *pbitmap, int height, int width, int bitsPerPixel, uint32_t fileSize, uint32_t PixelByteSize)
{
    strcpy(pbitmap->fileheader.signature,"BM");
    pbitmap->fileheader.filesize = fileSize;
    pbitmap->fileheader.fileoffset_to_pixelarray = sizeof(bitmap);
    pbitmap->bitmapinfoheader.dibheadersize =sizeof(bitmapinfoheader);
    pbitmap->bitmapinfoheader.width = width;
    pbitmap->bitmapinfoheader.height = height;
    pbitmap->bitmapinfoheader.planes = _planes;
    pbitmap->bitmapinfoheader.bitsperpixel = bitsPerPixel;
    pbitmap->bitmapinfoheader.compression = _compression;
    pbitmap->bitmapinfoheader.imagesize = PixelByteSize;
    pbitmap->bitmapinfoheader.ypixelpermeter = _ypixelpermeter;
    pbitmap->bitmapinfoheader.xpixelpermeter = _xpixelpermeter;
    pbitmap->bitmapinfoheader.numcolorspallette = 0;
}
void fileWrite(bitmap *arg_pbitmap, uint8_t *arg_pixelbuffer, FILE *fp, uint32_t PixelByteSize)
{
    fwrite(arg_pbitmap, 1, sizeof(bitmap),fp); // pbitmap(pointer to the structure), 1(size of element), sizeof(bitmap)(number of elements to write), file we are making operation on.
    memset(arg_pixelbuffer,pixel,PixelByteSize); // fills bites of memmory block with given value. Here, our value is our color. Turns out not really lol
    fwrite(arg_pixelbuffer,1,PixelByteSize,fp);
}

void clearMemmory(bitmap *arg_pbitmap, uint8_t *arg_pixelbuffer)
{
    free(arg_pbitmap);
    free(arg_pixelbuffer);
}

int main (int argc , char *argv[])
{
    int width = atoi(argv[1]);
    int height = atoi(argv[2]);
    int bitsperpixel = 24;

    FILE *fp = fopen("dlajacka2.bmp","wb");                                      //checks if file exists, if not, creates one.
    uint32_t PixelByteSize = getPixelByteSize(height, width, bitsperpixel);

    bitmap *pbitmap  = (bitmap*)calloc(1,sizeof(bitmap));                   //creates pointer that points to the beginning of allocated memmory which is size of bitmap structure(it sums up with fileheader and bitmapinfoheader)
    uint8_t *pixelbuffer = (uint8_t*)malloc(PixelByteSize);

    uint32_t fileSize = getFileSize(PixelByteSize);

    setBitmapInfoHeader(pbitmap, height, width, bitsperpixel, fileSize, PixelByteSize);
    fileWrite(pbitmap, pixelbuffer, fp, PixelByteSize);
    clearMemmory(pbitmap, pixelbuffer);

    fclose(fp);
}
