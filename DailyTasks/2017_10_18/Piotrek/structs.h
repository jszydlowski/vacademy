#ifndef STRUCTS_H
#define STRUCTS_H

struct BitmapFileHeader
{
    unsigned int   size; //rozmiar pliku
    unsigned short reserved1; //zarezerwowane
    unsigned short reserved2; //zarezerwowane
    unsigned int   offBits; //offset do danych
};

struct BitmapInfoHeader
{
    unsigned int size; //rozmiar headera
    int width; //szerokosc obrazka
    int height; //wysokosc obrazka
    unsigned short planes; //ilosc powierzchni (?)
    unsigned short bitCount; //ilosc bitow na pixel
    unsigned int compression; //rodzaj kompresji
    unsigned int imageSize; //rozmiar danych obrazka (?)
    int xPelsPerMeter; //X pikseli na metr ?
    int yPelsPerMeter; //Y pikseli na metr ?
    unsigned int clrUsed; //numer kolorow uzytych
    unsigned int clrImportant; //numer waznych kolorow
};

#endif // STRUCTS_H
