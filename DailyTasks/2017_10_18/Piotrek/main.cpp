#include <iostream>
#include <cstdlib>

#include "imagecreator.h"

bool checkInput (int argc, char* argv[])
{
    if (argc < 4)
    {
            std::cout << "Bad input" << std::endl;
            return false;
    }

    std::cout << "Good input" << std::endl;
    return true;
}

int main (int argc, char* argv[])
{
    if (checkInput(argc, argv))
    {
        ImageCreator imageCreator;

        if(argc == 4)
        {
            std::cout << "Default color" << std::endl;
             imageCreator.createImage(atoi(argv[1]), atoi(argv[2]), argv[3]);
        }
        else
        {
            std::cout << "Custom color" << std::endl;
            imageCreator.createImage(atoi(argv[1]), atoi(argv[2]), argv[3], argv[4]);
        }

        std::cout << "Image created" << std::endl;
        return 0;
    }
    else
    {
        std::cout << "Need more arguments" << std::endl;
        return -1;
    }
}
