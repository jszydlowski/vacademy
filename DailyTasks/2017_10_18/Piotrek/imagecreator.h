#ifndef IMAGECREATOR_H
#define IMAGECREATOR_H

#include <iostream>
#include <windows.h>
#include <vector>

#include "structs.h"

class ImageCreator
{
public:
    ImageCreator();
    void createImage(int imageWidth, int imageHeight, std::string filename, std::string inputColor = " ");
    std::vector<uint8_t> createDataArray(int imageWidth, int imageHeight, uint8_t R = 230, uint8_t G = 100, uint8_t B = 70);
    void convertToHex(std::string &tR, std::string &tG, std::string &tB, uint8_t &R, uint8_t &G, uint8_t &B);
    void sliceInputColor(const std::string &inputColor, std::string &tR, std::string &tG, std::string &tB);
    void saveFile(const BitmapFileHeader &fileHeader, const BitmapInfoHeader &infoHeader, const unsigned short &type, const std::string &filename, const std::vector<uint8_t> data);
};

#endif // IMAGECREATOR_H

