#include "imagecreator.h"

#include <cstdlib>
#include <sstream>

ImageCreator::ImageCreator()
{

}

void ImageCreator::createImage(int imageWidth, int imageHeight, std::string filename, std::string inputColor)
{
    BitmapFileHeader fileHeader;
    BitmapInfoHeader infoHeader;

    unsigned short type = 0x4D42;

    fileHeader.reserved1 = 0;
    fileHeader.reserved2 = 0;
    fileHeader.size = 2 + sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader) + imageWidth * imageHeight * 3;
    fileHeader.offBits = 0x36;

    infoHeader.size = sizeof(BitmapInfoHeader);
    infoHeader.width = imageWidth;
    infoHeader.height = imageHeight;
    infoHeader.planes = 1;
    infoHeader.bitCount = 8;
    infoHeader.compression = 0;
    infoHeader.imageSize = 0;
    infoHeader.xPelsPerMeter = 5000;
    infoHeader.yPelsPerMeter = 5000;
    infoHeader.clrUsed = 0;
    infoHeader.clrImportant = 0;

    if (inputColor == " ")
    {
        std::vector<uint8_t> data = createDataArray(imageWidth, imageHeight);
        saveFile(fileHeader, infoHeader, type, filename, data);
    }
    else
    {
        uint8_t R = 40;
        std::string tR = "";
        uint8_t G = 40;
        std::string tG = "";
        uint8_t B = 40;
        std::string tB = "";

        sliceInputColor(inputColor, tR, tG, tB);
        convertToHex(tR, tG, tB, R, G, B);
        std::vector<uint8_t> data = createDataArray(imageWidth, imageHeight, R, G, B);

        saveFile(fileHeader, infoHeader, type, filename, data);
    }
}

std::vector<uint8_t> ImageCreator::createDataArray(int imageWidth, int imageHeight, uint8_t R, uint8_t G, uint8_t B)
{
    const int DATA_SIZE = (imageHeight * imageWidth) * 4;
    std::vector<uint8_t> rawData;

    for (int i = 0; i < DATA_SIZE; i += 4)
    {
        rawData.push_back(B);
        rawData.push_back(G);
        rawData.push_back(R);
        rawData.push_back(0);
    }

    return rawData;
}

void ImageCreator::convertToHex(std::string &tR, std::string &tG, std::string &tB, uint8_t &R, uint8_t &G, uint8_t &B)
{
    std::stringstream stream1;
    std::stringstream stream2;
    std::stringstream stream3;

    stream1 << std::hex << tR;
    std::string tempR(stream1.str());
    R = std::stoul(tempR, nullptr, 16);
    stream1.clear();

    stream2 << std::hex << tG;
    std::string tempG(stream2.str());
    G = std::stoul(tempG, nullptr, 16);
    stream2.clear();

    stream3 << std::hex << tB;
    std::string tempB(stream3.str());
    B = std::stoul(tempB, nullptr, 16);
    stream3.clear();
}

void ImageCreator::sliceInputColor(const std::string &inputColor, std::string &tR, std::string &tG, std::string &tB)
{
    tR = inputColor.substr(0, 2);
    tG = inputColor.substr(2, 2);
    tB = inputColor.substr(4, 2);
}

void ImageCreator::saveFile(const BitmapFileHeader &fileHeader, const BitmapInfoHeader &infoHeader, const unsigned short &type, const std::string &filename, const std::vector<uint8_t> data)
{
    const std::string EXTENSION = ".bmp";
    const std::string FULL_FILENAME = filename + EXTENSION;

    FILE *file = fopen(FULL_FILENAME.c_str(), "wb");

    if (!file)
    {
        std::cout << "Cannot open the file" << std::endl;
        return;
    }

    fwrite(&type, 1, sizeof(type), file);
    fwrite(&fileHeader, 1, sizeof(fileHeader), file);
    fwrite(&infoHeader, 1, sizeof(infoHeader), file);

    const int DATA_SIZE = (infoHeader.height * infoHeader.width) * 4;

    for (int i = 0; i < DATA_SIZE; ++i)
    {
        fwrite(&data[i], 1, 1, file);
    }

    fclose(file);
}
