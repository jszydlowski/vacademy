#include <iostream>
#include <string>
#include <cstring>
#include <cmath>
#include <fstream>
#include <vector>
#include <ctime>
#include "stdlib.h"
#include <sstream>
#include <random>
#include <functional>

using namespace std;

#pragma pack(push, 1)

typedef struct tagBITMAPFILEHEADER {
	unsigned short bfType;
	unsigned int bfSize;
	unsigned short bfReserved1;
	unsigned short bfReserved2;
	unsigned int bfOffBits;
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFO {
	unsigned int biSize;
	int biWidth;
	int biHeight;
	unsigned short biPlanes;
	unsigned short biBitCount;
	unsigned int biCompression;
	unsigned int biSizeImage;
	int biXPelsPerMeter;
	int biYPelsPerMeter;
	unsigned int biClrUsed;
	unsigned int biClrImportant;
} BITMAPINFOHEADER;

#pragma pack(pop)

const string RANDOM = "RANDOM";

class BitMap {
private:
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	vector<uint8_t> imgBuffer;
	int width;
	int height;

public:

	BitMap(int, int, string);
	BITMAPFILEHEADER getBmfh();
	BITMAPINFOHEADER getBmih();
	vector<uint8_t> getImageBuffer();
	void writeIntoImgBuffer(string colour);
};

BitMap::BitMap(int width, int height, string hexColour) {

	this->width = width;
	this->height = height;

	int bitCount = 24;
	int rowLength = 4 * ((width * bitCount + 31) / 32);

	//Fill file header
	bmfh.bfType = 0x4D42; //= 'BM' -> means that it's a bitmap
	bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)
			+ rowLength * height;
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	//Fill info header
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biWidth = this->width;
	bmih.biHeight = this->height;
	bmih.biPlanes = 1;
	bmih.biBitCount = 24;
	bmih.biCompression = 0; //BI_RGB = 0
	bmih.biSizeImage = rowLength * height; // 0 for BI_RGB images
	bmih.biXPelsPerMeter = 0;
	bmih.biYPelsPerMeter = 0;
	bmih.biClrUsed = 0;
	bmih.biClrImportant = 0;

	writeIntoImgBuffer(hexColour);
}

void BitMap::writeIntoImgBuffer(string hexColour) {

	int bitCount = 24;
	int rowLength = 4 * ((width * bitCount + 31) / 32);
	stringstream streamR, streamG, streamB;

	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> dis(0, 256);
	auto res = bind(dis, gen);

	if (hexColour != RANDOM) {
		string redString = hexColour.substr(0, 2);
		streamR << std::hex << redString;
		string tString = streamR.str();
		uint8_t redU8 = stoul(tString, nullptr, 16);
		streamR.clear();

		string greenString = hexColour.substr(2, 2);
		streamG << std::hex << greenString;
		tString = streamG.str();
		uint8_t greenU8 = stoul(tString, nullptr, 16);
		streamG.clear();

		string blueString = hexColour.substr(4, 2);
		streamB << std::hex << blueString;
		tString = streamB.str();
		uint8_t blueU8 = stoul(tString, nullptr, 16);
		streamB.clear();

		for (int i = 0; i < this->height; i++) {
			int j = 0;
			while (j < rowLength) {
				if (j >= this->width * 3) {
					imgBuffer.push_back(0);
					j++;
				} else {
					imgBuffer.push_back(blueU8);
					imgBuffer.push_back(greenU8);
					imgBuffer.push_back(redU8);
					j += 3;
				}
			}
		}
	} else {
		for (int i = 0; i < this->height; i++) {
			int j = 0;
			while (j < rowLength) {
				if (j >= this->width * 3) {
					imgBuffer.push_back(0);
					j++;
				} else {
					uint8_t blueU8 = res();
					uint8_t greenU8 = res();
					uint8_t redU8 = res();
					imgBuffer.push_back(blueU8);
					imgBuffer.push_back(greenU8);
					imgBuffer.push_back(redU8);
					j += 3;
				}
			}
		}
	}
}

BITMAPFILEHEADER BitMap::getBmfh() {
	return bmfh;
}

BITMAPINFOHEADER BitMap::getBmih() {
	return bmih;
}

vector<uint8_t> BitMap::getImageBuffer() {
	return imgBuffer;
}

void saveBitMap(BitMap bmp, string fileName) {

	fileName += ".bmp";

	const BITMAPFILEHEADER bfh = bmp.getBmfh();
	const BITMAPINFOHEADER bih = bmp.getBmih();

	FILE *bmpFile = fopen(fileName.c_str(), "wb");

	if (bmpFile) {
		fwrite(&bfh, 1, sizeof(BITMAPFILEHEADER), bmpFile);
		fwrite(&bih, 1, sizeof(BITMAPINFOHEADER), bmpFile);

		for (auto num : bmp.getImageBuffer()) {
			fwrite(&num, 1, 1, bmpFile);
		}

		fclose(bmpFile);
		cout << "File saved" << endl;
	} else
		cout << "Unable to open file";
}

int main(int argc, char *argv[]) {

	if (argc < 4) {
		cout << "Write at least 3 parameters (width, height, fileName). 4th parameter Colour is optional" << endl;
		return -1;
	} else if (argc == 4) {
		BitMap bmp(stoi(argv[1]), stoi(argv[2]), RANDOM);
		string name = argv[3];
		saveBitMap(bmp, name);
		return 0;
	} else if (argc == 5) {
		string colour(argv[4]);
		BitMap bmp(stoi(argv[1]), stoi(argv[2]), colour);
		string name = argv[3];
		saveBitMap(bmp, name);
		return 0;
	} else {
		cout << "Too many arguments!" << endl;
		return -1;
	}

}

