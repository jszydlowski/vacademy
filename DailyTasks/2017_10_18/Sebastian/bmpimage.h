#ifndef BMPIMAGE_H
#define BMPIMAGE_H

#include <string>
#include <vector>

class BMPImage
{
public:
    BMPImage(int width, int height, std::string fileName, std::string color);
    ~BMPImage();

    void saveToFile();

private:
    struct Color
    {
        unsigned char red = 0;
        unsigned char green = 0;
        unsigned char blue = 0;

        bool random = false;
    };

#pragma pack(push, 1)
    struct FileHeader
    {
        short type = 0x4D42; // BM
        unsigned int fileSize;
        short reserved1 = 0;
        short reserved2 = 0;
        int offset = 54;

        FileHeader(int imageSize = 0)
        {
            fileSize = offset + imageSize;
        }
    };

    struct InfoHeader
    {
        int size = 40;
        unsigned int width;
        unsigned int height;
        short planes = 1;
        short bitsPerPixel = 24;
        int compression = 0;
        unsigned int imageSize;
        int xResolution = 0;
        int yResolution = 0;
        int colorsUsed = 0;
        char colorsImportant = 0;
        char rotation = 0;
        short reserved = 0;

        InfoHeader(unsigned int _width = 0, unsigned int _height = 0, int padding = 0) :
            width(_width), height(_height)
        {
            const int BITS_IN_BYTE = 8;
            imageSize = height * ( ( width * (bitsPerPixel / BITS_IN_BYTE) ) + padding );
        }
    };
#pragma pack(pop)


    int m_width;
    int m_height;
    int m_size;
    std::string m_fileName;
    Color m_color;
    int m_padding;
    InfoHeader m_infoHeader;
    FileHeader m_fileHeader;
    std::vector<unsigned char> m_data;

    Color stringToColor(const std::string& value) const;
};

#endif // BMPIMAGE_H
