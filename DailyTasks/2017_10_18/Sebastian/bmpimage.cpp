#include "bmpimage.h"

#include <iostream>
#include <fstream>
#include <ctime>

const int PIXEL_COMPONENT_NUMBER = 3;
const std::string FILE_EXTENSION = ".bmp";

BMPImage::BMPImage(int width, int height, std::string fileName, std::string color) :
    m_width(width),
    m_height(height),
    m_size(m_width * m_height),
    m_fileName(fileName + FILE_EXTENSION),
    m_color(stringToColor(color)),
    m_padding((4 - ((m_width * PIXEL_COMPONENT_NUMBER) % 4)) % 4),
    m_infoHeader(InfoHeader(m_width, m_height, m_padding)),
    m_fileHeader(FileHeader(m_infoHeader.imageSize))
{
    srand(time(nullptr));
    m_data.resize(m_infoHeader.imageSize);

    int colorBytesInRow = m_width * PIXEL_COMPONENT_NUMBER;
    int totalBytesInRow = colorBytesInRow + m_padding;
    int pos = 0;

    for (int y = 0; y < m_height; ++y)
    {
        for (int x = 0; x < colorBytesInRow; x += PIXEL_COMPONENT_NUMBER)
        {
            pos = (m_height - y - 1) * totalBytesInRow + x;

            if (m_color.random)
            {
                const int MAX_VALUE = 255;

                m_color.blue = rand() % MAX_VALUE;
                m_color.green = rand() % MAX_VALUE;
                m_color.red = rand() % MAX_VALUE;
            }

            m_data[pos] = m_color.blue;
            m_data[pos + 1] = m_color.green;
            m_data[pos + 2] = m_color.red;
        }
    }
}

BMPImage::~BMPImage()
{

}

void BMPImage::saveToFile()
{
    std::ofstream file(m_fileName, std::ios::binary);

    file.write((char*) &m_fileHeader, sizeof(m_fileHeader));
    file.write((char*) &m_infoHeader, sizeof(m_infoHeader));

    for (unsigned int i = 0; i < m_data.size(); ++i)
    {
        file.write((char*) &m_data[i], sizeof(unsigned char));
    }
}

BMPImage::Color BMPImage::stringToColor(const std::string &value) const
{
    Color result;
    if (value.empty())
    {
        result.random = true;

        return result;
    }


    const int START_POS = 0;
    const int LENGTH = 2;
    std::size_t* const CHARS_PROCESSED = nullptr;
    const int BASE = 16;

    result.red =    stoi(value.substr(START_POS,     LENGTH), CHARS_PROCESSED, BASE);
    result.green =  stoi(value.substr(START_POS + 2, LENGTH), CHARS_PROCESSED, BASE);
    result.blue =   stoi(value.substr(START_POS + 4, LENGTH), CHARS_PROCESSED, BASE);

    return result;
}

