#include <iostream>
#include <string>

#include "bmpimage.h"

enum CmdArguments
{
    WIDTH = 1,
    HEIGHT,
    FILENAME,
    COLOR
};

const int MIN_ARGC = 4;
const int MAX_ARGC = 5;

bool validateInputData(int paramCount, char *data[])
{
    if (paramCount < MIN_ARGC || paramCount > MAX_ARGC)
    {
        std::cout << "Invalid number of arguments!" << std::endl;
        std::cout << "Arguments list:" << std::endl;
        std::cout << "1. width" << std::endl;
        std::cout << "2. height" << std::endl;
        std::cout << "3. filename" << std::endl;
        std::cout << "4. color (optional) (example: A1C2E3)" << std::endl;

        return false;
    }

    //validate 'width' parameter
    if (atoi(data[WIDTH]) <= 0)
    {
        std::cout << "1st parameter must be greater than 0!" << std::endl;
        return false;
    }

    char* lastCharacter = nullptr;
    int base = 10;

    strtol(data[WIDTH], &lastCharacter, base);
    if (*lastCharacter)
    {
        std::cout << "1st parameter can't contain any letters!" << std::endl;
        return false;
    }

    //validate 'heigth' parameter
    if (atoi(data[HEIGHT]) <= 0)
    {
        std::cout << "2nd parameter must be greater than 0!" << std::endl;
        return false;
    }

    lastCharacter = nullptr;
    strtol(data[HEIGHT], &lastCharacter, base);
    if (*lastCharacter)
    {
        std::cout << "2nd parameter can't contain any letters!" << std::endl;
        return false;
    }

    // validate 'color' parameter
    if (paramCount == MAX_ARGC)
    {
        std::string color(data[COLOR]);
        const int MAX_COLOR_LENGTH = 6;

        if (color.length() != MAX_COLOR_LENGTH)
        {
            std::cout << "Invalid color argument!" << std::endl;
            return false;
        }

        const int COLOR_COMPONENTS = 2;

        for (unsigned int i = 0; i < color.length(); i += COLOR_COMPONENTS)
        {
            lastCharacter = nullptr;

            std::string value = "0x";
            value += color.substr(i, COLOR_COMPONENTS);

            base = 16;

            strtol(color.c_str(), &lastCharacter, base);
            if (*lastCharacter)
            {
                std::cout << "Invalid color argument!" << std::endl;
                return false;
            }
        }
    }

    return true;
}

int main(int argc, char *argv[])
{
    if (!validateInputData(argc, argv))
        return 0;

    int width = atoi(argv[WIDTH]);
    int height = atoi(argv[HEIGHT]);
    std::string fileName { argv[FILENAME] };
    std::string color { argc == MAX_ARGC ? argv[COLOR] : "" };

    BMPImage image(width, height, fileName, color);

    image.saveToFile();

    return 0;
}
