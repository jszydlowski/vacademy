#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include <time.h>

#define BITS_PER_PIXEL (24)
#define PLANES (1)
#define COMPRESSION (0)
#define PIXEL_BYTE_SIZE (height * width * BITS_PER_PIXEL / 8)
#define FILE_SIZE PIXEL_BYTE_SIZE + sizeof(bitmap)
#define _xpixelpermeter 0x130B //2835 , 72 DPI
#define _ypixelpermeter 0x130B //2835 , 72 DPI

// global variables
int width = 0;
int height = 0;

#pragma pack(push, 1)

typedef struct
{
    char signature[2];
    uint32_t filesize;
    uint32_t reserved;
    uint32_t fileoffset_to_pixelarray;
} fileheader;

typedef struct
{
    uint32_t dibheadersize;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitsperpixel;
    uint32_t compression;
    uint32_t imagesize;
    uint32_t ypixelpermeter;
    uint32_t xpixelpermeter;
    uint32_t numcolorspallette;
    uint32_t mostimpcolor;
} bitmapinfoheader;

typedef struct
{
    fileheader fileheader;
    bitmapinfoheader bitmapinfoheader;
} bitmap;

#pragma pack(pop)

void prepareHeaderFile(bitmap*);
void prepareBmpFile(int*, uint8_t*, uint8_t*, uint8_t*, uint8_t*);
void saveFile(char**, const bitmap *, const uint8_t *);
void calcColors(uint8_t*, uint8_t*, uint8_t*, int);
void clear(bitmap*, uint8_t*, char*);

int main(int argc, char **argv)
{
    char *outputFileName = new char;
    int color     = 0;
    uint8_t blue  = 0;
    uint8_t green = 0;
    uint8_t red   = 0;

    if(argc > 1)
    {
        width = atoi(argv[1]);             // save width
        height = atoi(argv[2]);           // save height
        outputFileName = argv[3];        // save output filename
    }

    if(argc > 4) // if color is provided as argument
    {
        sscanf(argv[4], "%x", &color); // save hex to int
    }

    else // default values
    {
        width = 500;
        height = 500;
        strcpy(outputFileName, "file");
        color = 255;
    }

    // allocate memory for bitmap and pixelBuffer
    bitmap *pbitmap  = (bitmap*)calloc(1, sizeof(bitmap));
    uint8_t *pixelBuffer = (uint8_t*)malloc(PIXEL_BYTE_SIZE);

    calcColors(&blue, &green, &red, color);
    prepareHeaderFile(pbitmap);
    prepareBmpFile(&argc, pixelBuffer, &blue, &green, &red);
    saveFile(&outputFileName, pbitmap, pixelBuffer);

    clear(pbitmap, pixelBuffer, outputFileName);

    return 0;
}

void clear(bitmap *pbitmap, uint8_t *pixelBuffer, char *outputFileName)
{
    // clear pointers
    free(pbitmap);
    pbitmap = 0x00;

    free(pixelBuffer);
    pixelBuffer = 0x00;

    free(outputFileName);
    outputFileName = 0x00;
}

void prepareHeaderFile(bitmap *pbitmap)
{
    // header of bitmap file
    strcpy(pbitmap->fileheader.signature,"BM");
    pbitmap->fileheader.filesize = FILE_SIZE;
    pbitmap->fileheader.fileoffset_to_pixelarray = sizeof(bitmap);
    pbitmap->bitmapinfoheader.dibheadersize =sizeof(bitmapinfoheader);
    pbitmap->bitmapinfoheader.width = width;
    pbitmap->bitmapinfoheader.height = height;
    pbitmap->bitmapinfoheader.planes = PLANES;
    pbitmap->bitmapinfoheader.bitsperpixel = BITS_PER_PIXEL;
    pbitmap->bitmapinfoheader.compression = COMPRESSION;
    pbitmap->bitmapinfoheader.imagesize = PIXEL_BYTE_SIZE;
    pbitmap->bitmapinfoheader.ypixelpermeter = _ypixelpermeter ;
    pbitmap->bitmapinfoheader.xpixelpermeter = _xpixelpermeter ;
    pbitmap->bitmapinfoheader.numcolorspallette = 0;
}

void prepareBmpFile(int *argc, uint8_t *pixelBuffer, uint8_t *blue, uint8_t *green, uint8_t *red)
{
    // write colors for pixel to pixelBuffer using loop unrolling
    if(*argc >= 4)
    {
        for(int i = 0; i < PIXEL_BYTE_SIZE; i += 3)
        {
            //first pixel
            pixelBuffer[i    ] = *blue;
            pixelBuffer[i + 1] = *green;
            pixelBuffer[i + 2] = *red;

    //        //second pixel
    //        pixelBuffer[i + 3] = *red;
    //        pixelBuffer[i + 4] = *blue;
    //        pixelBuffer[i + 5] = *green;

    //        //third pixel
    //        pixelBuffer[i + 6] = *green;
    //        pixelBuffer[i + 7] = *red;
    //        pixelBuffer[i + 8] = *red;
        }
    }

    else
    {
        srand(time(NULL));
        for(int i = 0; i < PIXEL_BYTE_SIZE; i += 3)
        {
            //first pixel
            pixelBuffer[i    ] = rand() % 255;
            pixelBuffer[i + 1] = rand() % 255;
            pixelBuffer[i + 2] = rand() % 255;
        }
    }
}

void calcColors(uint8_t *blue, uint8_t *green, uint8_t *red, int color)
{
    // calculate colors
    *blue  = (color & 0xFF);
    *green = ((color >> 8) & 0xFF);
    *red   = ((color >> 16) & 0xFF);
}

void saveFile(char **outputFileName, const bitmap *pbitmap, const uint8_t *pixelBuffer)
{
    // create destination file
    strcat(*outputFileName, ".bmp"); // add .bmp to the file name
    FILE *bmpFile = fopen(*outputFileName, "wb");

    // write header of bitmap file to destiation file
    fwrite (pbitmap, 1, sizeof(bitmap), bmpFile);

    // write filled pixelBuffer to the destination file
    fwrite(pixelBuffer, 1, PIXEL_BYTE_SIZE, bmpFile);

    //close stream
    fclose(bmpFile);
}
