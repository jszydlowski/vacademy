#ifndef BMP_MAKER_H
#define BMP_MAKER_H

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <string.h>
#include <malloc.h>
#include <String>

#define pixel 0xAAA


class bmp_maker
{

public:

    bmp_maker();
    ~bmp_maker();
    void saveToFile(std::string fileName);
    bool createPicture(int m_width , int m_height , std::string m_colorHex);


#pragma pack(push,1)
    typedef struct{
        uint8_t signature[2];
        uint32_t filesize;
        uint32_t reserved;
        uint32_t fileoffset_to_pixelarray;
    } fileheader;
    typedef struct{
        uint32_t dibheadersize;
        uint32_t width;
        uint32_t height;
        uint16_t planes;
        uint16_t bitsperpixel;
        uint32_t compression;
        uint32_t imagesize;
        uint32_t ypixelpermeter;
        uint32_t xpixelpermeter;
        uint32_t numcolorspallette;
        uint32_t mostimpcolor;
    } bitmapinfoheader;
    typedef struct {
        fileheader fileheader;
        bitmapinfoheader bitmapinfoheader;
    } bitmap;
#pragma pack(pop)

    FILE *m_fp;
    bitmap *m_pbitMap;
    uint8_t *m_pixelBuffer;
    int m_height  = 400;
    int m_width = 400;
    int m_pixelbytesize;
    int  m_filesize;
    std::string m_colorHex;
    const char *charPtr;

};

#endif // BMP_MAKER_H
