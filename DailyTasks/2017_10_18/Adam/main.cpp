#include <iostream>
#include <qdebug>
#include <Windows.h>
#include <algorithm>
#include <memory>
#include "bmp_maker.h"



int main(int argc, char *argv[])
{
    std::string color;

    if(argc == 5)
    {
      color = argv[4];
    }
    else
    {
        color = " ";
    }

    unsigned int width = atoi(argv[1]);
    unsigned int height = atoi(argv[2]);
    std::string fileName { argv[3] };

    bmp_maker ob;
    ob.createPicture(width , height , color);
    ob.saveToFile(fileName);


    return 0;
}



