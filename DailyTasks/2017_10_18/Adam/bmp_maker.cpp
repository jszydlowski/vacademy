#include "bmp_maker.h"


using namespace std;

bmp_maker::bmp_maker()
{

    m_pbitMap  = new bitmap[sizeof(bitmap)];

    m_pbitMap->fileheader.signature[0] ='B';
    m_pbitMap->fileheader.signature[1] ='M';
    m_pbitMap->fileheader.fileoffset_to_pixelarray = sizeof(bitmap);
    m_pbitMap->bitmapinfoheader.dibheadersize =sizeof(bitmapinfoheader);
    m_pbitMap->bitmapinfoheader.planes = 1;
    m_pbitMap->bitmapinfoheader.bitsperpixel = 24;
    m_pbitMap->bitmapinfoheader.compression = 0;
    m_pbitMap->bitmapinfoheader.ypixelpermeter = 0x130B ;
    m_pbitMap->bitmapinfoheader.xpixelpermeter = 0x130B ;
    m_pbitMap->bitmapinfoheader.numcolorspallette = 0;
    m_pbitMap->bitmapinfoheader.mostimpcolor = 0;



}

void bmp_maker::saveToFile(string fileName)
{
    fileName += ".bmp";

    charPtr = new char[fileName.length()];
    charPtr = fileName.c_str();

    m_fp = fopen(charPtr ,"wb");
    fwrite (m_pbitMap, 1, sizeof(bitmap),m_fp);
    fwrite(m_pixelBuffer,1,m_pixelbytesize,m_fp);
    fclose(m_fp);

    delete charPtr;

}

bool bmp_maker::createPicture(int m_width ,int m_height , std::string m_colorHex)
{
    if(m_width < 1 || m_height < 1){
        return 0;
    }
    m_pixelbytesize = m_height*m_width* 24/8;
    m_filesize = m_pixelbytesize+sizeof(bitmap);
    m_pixelBuffer = new uint8_t[m_pixelbytesize];
    m_pbitMap->fileheader.filesize = m_filesize;
    m_pbitMap->bitmapinfoheader.width = m_width;
    m_pbitMap->bitmapinfoheader.height = m_height;
    m_pbitMap->bitmapinfoheader.imagesize = m_pixelbytesize;


    if(m_colorHex.size() != 6)
    {
        for(int i = 0 ; i < m_pixelbytesize; i += 3)
        {
            m_pixelBuffer[i]     = ( std::rand() % 255 );
            m_pixelBuffer[i + 1] = ( std::rand() % 255 );
            m_pixelBuffer[i + 2] = ( std::rand() % 255 );
        }
        return 0;


    }
    else
    {

        for(int i = 0 ; i < m_pixelbytesize; i += 3)
        {
            std::string rString = m_colorHex.substr(0,2);
            std::string gString = m_colorHex.substr(2,2);
            std::string bString = m_colorHex.substr(4,2);

            int r = std::stoul(rString, nullptr, 16);
            int g = std::stoul(gString, nullptr, 16);
            int b = std::stoul(bString, nullptr, 16);

            m_pixelBuffer[i]     = b;
            m_pixelBuffer[i + 1] = g;
            m_pixelBuffer[i + 2] = r;
        }

    }

    return 1;
}

bmp_maker::~bmp_maker()
{
    delete m_pbitMap;
    delete m_pixelBuffer;
}


