#include "gameboard.h"

#include <iostream>

GameBoard::GameBoard()
{
       m_board[1] = '1';
       m_board[2] = '2';
       m_board[3] = '3';
       m_board[4] = '4';
       m_board[5] = '5';
       m_board[6] = '6';
       m_board[7] = '7';
       m_board[8] = '8';
       m_board[9] = '9';

       m_availableSpace[1] = 1;
       m_availableSpace[2] = 2;
       m_availableSpace[3] = 3;
       m_availableSpace[4] = 4;
       m_availableSpace[5] = 5;
       m_availableSpace[6] = 6;
       m_availableSpace[7] = 7;
       m_availableSpace[8] = 8;
       m_availableSpace[9] = 9;
}

void GameBoard::drawBoard()
{
    for (int i = 0; i < 5; ++i)
    {
        std::cout << "" << std::endl;
    }

    std::cout << "\t\t\t\t    |   |" << std::endl;
    std::cout << "\t\t\t\t  " << m_board[1] << " | " << m_board[2] << " | " << m_board[3] << std::endl;
    std::cout << "\t\t\t\t----+---+----" << std::endl;
    std::cout << "\t\t\t\t  " << m_board[4] << " | " << m_board[5] << " | " << m_board[6] << std::endl;
    std::cout << "\t\t\t\t----+---+----" << std::endl;
    std::cout << "\t\t\t\t  " << m_board[7] << " | " << m_board[8] << " | " << m_board[9] << std::endl;
    std::cout << "\t\t\t\t    |   |" << std::endl;

    for (int i = 0; i < 7; ++i)
    {
        std::cout << "" << std::endl;
    }
}

void GameBoard::printAvailableSpaces()
{
    for (auto i : m_availableSpace)
    {
        std::cout << i.second << "\t";
    }

    std::cout << "" << std::endl;
}

bool GameBoard::checkUserSpaceChoose(const int &userChoose)
{
    for (auto index : m_availableSpace)
    {
        if (index.second == userChoose)
        {
            return false;
        }
    }

    return true;
}

void GameBoard::placeUserCharacter(const char &character, const int &place)
{
    m_board[place] = character;
}

void GameBoard::reduceAvailableSpace(const int &place)
{
    m_availableSpace.erase(place);
}

bool GameBoard::checkOccupiedField()
{
    const unsigned CHARS_FOR_WIN = 3;
    const unsigned SIZE = m_board.size() - CHARS_FOR_WIN;

    for (unsigned i = 1; i <= SIZE; i += CHARS_FOR_WIN)
    {
        if ((m_board[i] == 'x') && (m_board[i + 1] == 'x') && (m_board[i + 2] =='x'))
        {
            return true;
        }
        if ((m_board[i] == 'o') && (m_board[i + 1] == 'o') && (m_board[i + 2] =='o'))
        {
            return true;
        }
    }

    for (unsigned i = 1; i <= SIZE; i += 2)
    {
        if ((m_board[i] == 'x') && (m_board[i + CHARS_FOR_WIN] == 'x') && (m_board[i + (CHARS_FOR_WIN * 2)] == 'x'))
        {
            return true;
        }
        if ((m_board[i] == 'o') && (m_board[i + CHARS_FOR_WIN] == 'o') && (m_board[i + (CHARS_FOR_WIN * 2)] == 'o'))
        {
            return true;
        }
    }

    if ((m_board[1] == 'x') && (m_board[5] == 'x') && (m_board[9] == 'x'))
    {
        return true;
    }
    if ((m_board[1] == 'o') && (m_board[5] == 'o') && (m_board[9] == 'o'))
    {
        return true;
    }

    if ((m_board[3] == 'x') && (m_board[5] == 'x') && (m_board[7] == 'x'))
    {
        return true;
    }
    if ((m_board[3] == 'o') && (m_board[5] == 'o') && (m_board[7] == 'o'))
    {
        return true;
    }

    return false;
}

