#include "player.h"

Player::Player() :
    m_playerNickname(" "),
    m_playerCharacter(' ')
{

}

void Player::setPlayerNick(const std::string &nickFromInput)
{
    m_playerNickname = nickFromInput;
}

void Player::setPlayerCharacter(const char &character)
{
    m_playerCharacter = character;
}

char Player::placeCharacter()
{
    return m_playerCharacter;
}

std::string Player::getPlayerName() const
{
    return m_playerNickname;
}
