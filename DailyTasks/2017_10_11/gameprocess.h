#ifndef GAMEPROCESS_H
#define GAMEPROCESS_H

#include "player.h"
#include "gameboard.h"

#include <memory>

/**
 * @brief The GameProcess class -> class which contains game logic.
 */
class GameProcess
{
public:
    /**
     * @brief GameProcess -> constructor which initializes unique_ptrs.
     */
    GameProcess();

    /**
     * @brief createPlayers -> method that creates two players.
     */
    void createPlayers();
    /**
     * @brief startGame -> method which contains the sequence of game.
     */
    void startGame();
    /**
     * @brief clearScreen -> method that clears the screen.
     */
    void clearScreen() const;
    /**
     * @brief showTitleScreen -> method that prints name of the game
     * at the beginning of the program.
     */
    void showTitleScreen() const;

private:
    /**
     * @brief m_playerOne -> pointer to first player instance.
     */
    std::unique_ptr<Player> m_playerOne = nullptr;
    /**
     * @brief m_playerTwo -> pointer to second player instance.
     */
    std::unique_ptr<Player> m_playerTwo = nullptr;
    /**
     * @brief m_gameBoard -> pointer to gameboard instance.
     */
    std::unique_ptr<GameBoard> m_gameBoard = nullptr;

    /**
     * @brief checkUserCharacterInput -> method that check if user input
     * at the beginning of the program is valid.
     * @param userInput -> user input.
     */
    void checkUserCharacterInput(const char &userInput);
    /**
     * @brief guessCharacterForUserTwo -> method that adjusts character
     * for second player.
     * @return -> character for player.
     */
    char guessCharacterForUserTwo() const;
    /**
     * @brief askPlayerForMovement -> method that asks user where in gameboard
     * he wants to put his character.
     * @param playerNumber -> number of field in gameboard.
     */
    void askPlayerForMovement(const int &playerNumber);
    /**
     * @brief showEndGameScreen -> method that ends the game.
     * @param playerName -> nickname of player that have won the game.
     */
    void showEndGameScreen(const std::string &playerName) const;
};

#endif // GAMEPROCESS_H
