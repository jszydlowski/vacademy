#include <iostream>
#include <cstdlib>

#include "gameprocess.h"

int main()
{
    GameProcess gameProcess;
    gameProcess.showTitleScreen();

    getchar();

    gameProcess.clearScreen();
    gameProcess.createPlayers();
    gameProcess.startGame();

    return 0;
}
