#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <memory>
#include <vector>
#include <map>

/**
 * @brief The GameBoard class -> class that represents game board.
 */
class GameBoard
{
public:
    /**
     * @brief GameBoard -> constructor which, initializes m_board and
     * m_availableSpace.
     */
    GameBoard();

    /**
     * @brief drawBoard -> method that prints current state of board.
     */
    void drawBoard();
    /**
     * @brief printAvailableSpaces -> method that prints all places in
     * board without characters.
     */
    void printAvailableSpaces();
    /**
     * @brief checkUserSpaceChoose -> method that checks if user passed
     * valid position.
     * @param userChoose -> position that user choosed.
     * @return -> true if position is available.
     */
    bool checkUserSpaceChoose(const int &userChoose);
    /**
     * @brief placeUserCharacter -> method that sets value into
     * m_board map.
     * @param character -> character from player - x or o.
     * @param place -> number of field in boardmap.
     */
    void placeUserCharacter(const char &character, const int &place);
    /**
     * @brief reduceAvailableSpace -> method that removes from
     * m_availableSpace number of fields that are in use.
     * @param place -> number of field in boardmap.
     */
    void reduceAvailableSpace(const int &place);
    /**
     * @brief checkOccupiedField -> method that checks if player win.
     * @return  -> true if win, false if not yet.
     */
    bool checkOccupiedField();

private:
    /**
     * @brief m_board -> map containing spaces in gameboard.
     */
    std::map<int, char> m_board;
    /**
     * @brief m_availableSpace -> map containing fields to
     * which user may indicate.
     */
    std::map<int, int> m_availableSpace;
};

#endif // GAMEBOARD_H
