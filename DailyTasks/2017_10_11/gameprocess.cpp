#include "gameprocess.h"

#include <algorithm>
#include <cstdlib>

GameProcess::GameProcess()
{
    m_playerOne = std::make_unique<Player>();
    m_playerTwo = std::make_unique<Player>();
    m_gameBoard = std::make_unique<GameBoard>();
}

void GameProcess::createPlayers()
{
    std::string tempName = "";
    char tempCharacter = ' ';

    std::cout << "\t\t\tWrite player one nickname" << std::endl;
    std::cin >> tempName;
    m_playerOne.get()->setPlayerNick(tempName);
    std::cout << std::endl;

    std::cout << "\t\t\tSet character for plyer one" << std::endl;
    std::cout << "\t\t\tType 'x' or 'o'" << std::endl;
    std::cin >> tempCharacter;
    checkUserCharacterInput(tempCharacter);
    std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;

    tempName.erase(tempName.begin(), tempName.end());

    std::cout << "\t\t\tWrite player two nickname" << std::endl;
    std::cin >> tempName;
    m_playerTwo.get()->setPlayerNick(tempName);
    std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;;
    std::cout << "\t\tCharacter for player two is setted automaticly" << std::endl;
    m_playerTwo.get()->setPlayerCharacter(guessCharacterForUserTwo());
    std::cout << "" << std::endl;

    getchar();
    getchar();
}

void GameProcess::startGame()
{
    for (int i = 0; i < 9; ++i)
    {
        clearScreen();

        m_gameBoard.get()->drawBoard();

        askPlayerForMovement(i % 2);

        clearScreen();

        m_gameBoard.get()->drawBoard();
    }
}

void GameProcess::clearScreen() const
{
    for (int i = 0; i < 200; ++i)
    {
        std::cout << "" << std::endl;
    }
}

void GameProcess::checkUserCharacterInput(const char &userInput)
{
    if (userInput == 'x' || userInput == 'o')
    {
        m_playerOne.get()->setPlayerCharacter(userInput);
    }
    else
    {
        std::cout << "Wrong input" << std::endl;
        exit(0);
    }
}

char GameProcess::guessCharacterForUserTwo() const
{
    if (m_playerOne.get()->placeCharacter() == 'x')
    {
        return 'o';
    }
    else
    {
        return 'X';
    }
}

void GameProcess::askPlayerForMovement(const int &playerNumber)
{
    int userSpaceChoose = 0;
    bool shouldLoopRunAgain = true;

    while(shouldLoopRunAgain)
    {
        std::cout << "\t\t   Where you want to put your character?" << std::endl;
        m_gameBoard.get()->printAvailableSpaces();

        std::cin >> userSpaceChoose;
        shouldLoopRunAgain = m_gameBoard.get()->checkUserSpaceChoose(userSpaceChoose);
    }

    if (playerNumber == 0)
    {
        m_gameBoard.get()->placeUserCharacter(m_playerOne.get()->placeCharacter(), userSpaceChoose);
        m_gameBoard.get()->reduceAvailableSpace(userSpaceChoose);

        if (m_gameBoard.get()->checkOccupiedField())
        {
            showEndGameScreen(m_playerOne.get()->getPlayerName());
        }
    }
    else
    {
        m_gameBoard.get()->placeUserCharacter(m_playerTwo.get()->placeCharacter(), userSpaceChoose);
        m_gameBoard.get()->reduceAvailableSpace(userSpaceChoose);

        if (m_gameBoard.get()->checkOccupiedField())
        {
            showEndGameScreen(m_playerTwo.get()->getPlayerName());
        }
    }
}

void GameProcess::showEndGameScreen(const std::string &playerName) const
{
    const int HALF_WINDOW_SIZE = 13;

    for (int i = 0; i < HALF_WINDOW_SIZE * 2; ++i)
    {
        if (i == (HALF_WINDOW_SIZE))
        {
            std::cout << "\t\t\t\tAND THE WINNER IS" << std::endl;
            std::cout << std::endl << std::endl;
            std::cout << "\t\t\t\t" << playerName;
        }

        std::cout << std::endl;
    }

    getchar();
    getchar();

    exit(0);
}

void GameProcess::showTitleScreen() const
{
    const int HALF_WINDOW_SIZE = 13;

    for (int i = 0; i < HALF_WINDOW_SIZE; ++i)
    {
        if (i == (HALF_WINDOW_SIZE - 1))
        {
            std::cout << "\t\t\t\t  Tic Tac Toe" << std::endl;
        }

        std::cout << std::endl;
    }
}
