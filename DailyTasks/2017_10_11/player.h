#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>

/**
 * @brief The Player class -> class which represents player.
 */
class Player
{
public:
    /**
     * @brief Player -> constructor which sets nikname and characer to ' '.
     */
    Player();

    /**
     * @brief setPlayerNick -> method that sets nick for player.
     * @param nickFromInput -> nick from input.
     */
    void setPlayerNick(const std::string &nickFromInput);
    /**
     * @brief setPlayerCharacter -> method that sets character for player.
     * @param character -> character for player.
     */
    void setPlayerCharacter(const char &character);
    /**
     * @brief placeCharacter -> method that returns players character.
     * @return -> players character.
     */
    char placeCharacter();
    /**
     * @brief getPlayerName -> method that returns players nickname.
     * @return  -> players nickname.
     */
    std::string getPlayerName() const;

private:
    /**
     * @brief m_playerNickname -> players nickname.
     */
    std::string m_playerNickname;
    /**
     * @brief m_playerCharacter -> players character.
     */
    char m_playerCharacter;
};

#endif // PLAYER_H
