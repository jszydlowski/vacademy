#include <iostream>

void write_digit(int &x, int position, int value)
{
    if(position == 0)
    {
        int mask = 15  << 4;
        x = x & mask;
        x = x | value;
    }
    else
    {
        int mask = 15;
        x = x & mask;
        value = value  << 4;
        x = x | value;
    }
}

int read_digit(int x, int position)
{
    if(position == 0)
    {
        int mask = 15;
        x = x & mask;
        return x;
    }
    else
    {
        x = x >> 4;
        return x;
    }
}

int main(int argc, char *argv[])
{
    int x = 0;
    write_digit(x, 1, 5);
    std::cout<<read_digit(x, 1)<<" <--Should return 5"<<std::endl;

    write_digit(x, 1, 1);
    std::cout<<read_digit(x, 1)<<" <--Should return 1"<<std::endl;

    write_digit(x, 0, 2);
    std::cout<<read_digit(x, 0)<<" <--Should return 2"<<std::endl;
}
