#include <iostream>

void write_digit(int* x, int place, int value)
{
    if (place == 0)
    {
        int mask = 15 << 4;
        *x &= mask;
        *x |= value;
    }
    else if (place == 1)
    {
        int mask = 15;
        int temp = value & mask;
        temp <<= 4;
        *x |= temp;
    }
    else
    {
        std::cout << "Invalid second argument" << std::endl;
    }
}

int read_digit(int x, int place)
{
    int mask = 0;
    if (place == 0)
    {
        mask = 15;
        return x & mask;
    }
    else if (place == 1)
    {
        mask = 15 << 4;
        x &= mask;
        return x >> 4;
    }
    else
    {
        return -1; // error
    }
}

int main()
{
    int x = 0;
    write_digit(&x, 0, 6);
    write_digit(&x, 1, 3);
    std::cout << x << std::endl;
    std::cout << read_digit(x, 0) << std::endl;
    std::cout << read_digit(x, 1) << std::endl;

    return 0;
}
