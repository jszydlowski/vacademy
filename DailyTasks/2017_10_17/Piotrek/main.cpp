#include <iostream>

const uint8_t HALF_BITS_AMOUNT = 4;

void write_digit (uint8_t &digit, uint8_t value, const uint8_t &place)
{
    if (place == 0)
    {
        //clear
        digit &= ((0xFF) << HALF_BITS_AMOUNT);

        //or
        digit |= value;
    }
    else if (place == 1)
    {
        //clear
        digit &= ((0xFF) >> HALF_BITS_AMOUNT);

        //or
        digit |= (value << HALF_BITS_AMOUNT);
    }
    else
    {
        std::cout << "Wrong place" << std::endl;
    }
}

uint8_t read_digit ( uint8_t &digit, const uint8_t &place)
{
    if (place == 0)
    {
        return digit & 0xFF;
    }
    else if (place == 1)
    {
        auto temp = digit & (0xFF << HALF_BITS_AMOUNT);
        return temp >> HALF_BITS_AMOUNT;
    }
    else
    {
        std::cout << "Wrong place" << std::endl;
        return -1;
    }
}

int main()
{
    uint8_t testValue = 0;

    write_digit(testValue, 7, 0);
    write_digit(testValue, 1, 1);
    write_digit(testValue, 4, 0);


    std::cout << static_cast<int>(read_digit(testValue, 0)) << std::endl;
//    std::cout << std::hex << testValue << std::endl;

    printf("hex= %02x\n", testValue);

//    std::cout << read_digit(testValue, 0) << std::endl;

    return 0;
}
