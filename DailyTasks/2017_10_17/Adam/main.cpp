#include <iostream>
#include <qdebug>

void write(uint8_t *halfbyte,int place , int number);


int main()
{
    uint8_t halfbyte = 0;
    write(&halfbyte , 2, 1);

    std::cout <<  (int)halfbyte;
    return 0;
}
void write(uint8_t *halfbyte, int place , int number)
{
    //uint8_t temp = 0;
    uint8_t  mask = 0b11110000;

    if(place == 1)
    {
        uint8_t  mask = 0xF0;

        uint8_t temp = mask & (*halfbyte);

        *halfbyte = (temp | number);
    }
    if(place == 2)
    {
        uint8_t  mask = 0x0F;

        uint8_t temp = mask & (*halfbyte);

        *halfbyte = ((number << 4) | temp);
    }


}
