#include <stdio.h>
#include <stdint.h>

/*
 * Defines and global variables
 */
#define HALFBYTE_SIZE   (4)
#define HALFBYTE_MASK   (0xF)
#define HALFBYTE_R      (HALFBYTE_MASK << HALFBYTE_SIZE)
#define HALFBYTE_L      (HALFBYTE_MASK)
static int test_counter;

/*
 * prototypes of functions
 */
void setHalfByte(uint8_t *byte, uint8_t position, uint8_t value);
uint8_t getHalfByte(uint8_t byte, uint8_t position);
void assertP(uint8_t byte, uint8_t position, uint8_t value, uint8_t expected);


int main()
{

    uint8_t firstNumber = 6;
    uint8_t secondNumber = 3;
    uint8_t storage = 215;
    uint8_t position = 0;

    position = 0;
    setHalfByte(&storage, position, firstNumber);

    position = 1;
    setHalfByte(&storage, position, secondNumber);

    printf("storage should have value of 54, and have: %d\n", storage);
    printf("First halfbyte set to: %d\n", getHalfByte(storage, 0));
    printf("Second halfbyte set to: %d\n", getHalfByte(storage, 1));

    //test first halfbyte
    assertP(0xAA, 0, 5, 5); // halfbyte, position, value, expected
    assertP(0xAA, 0, 7, 7);
    assertP(0xFF, 0, 5, 5);
    assertP(0xFF, 0, 0, 0);

    //test second halfbyte
    assertP(0xAA, 1, 5, 5); // halfbyte, position, value, expected
    assertP(0xAA, 1, 7, 7);
    assertP(0xFF, 1, 5, 5);
    assertP(0xFF, 1, 0, 0);

    //test random values (should not pass tests)
    assertP(0xAA, 1, 4, 10); // halfbyte, position, value, expected
    assertP(0xAA, 0, 7, 99);
    assertP(0xFF, 1, 6, 12);
    assertP(0xFF, 0, 8, 55);
    return 0;
}

/*
 * if position is st to 0, then change only first halfbyte (range from 0 to 3),
 * otherwise if position is se to 1, change only second halfbyte (range from 4 to 7)
 * INPUT:
 * *byte: pointer to storage for two halfbytes.
 * position: position of halfbyte to change
 * value: new value to store
 * OUTPUT:
 * stored value of halfbyte
 */
void setHalfByte(uint8_t *byte, uint8_t position, uint8_t value)
{
    if(position)
    {
        *byte = ((*byte & HALFBYTE_L) | value << HALFBYTE_SIZE);
    }

    else
    {
        *byte = ((*byte & HALFBYTE_R) | value);
    }
}

/*
 * print stored halfbyte
 * INPUT:
 * byte: copy of storage for two halfbytes.
 * position: position of halfbyte to change
 * OUTPUT:
 * computed value of halfbyte
 */
uint8_t getHalfByte(uint8_t byte, uint8_t position)
{
    if(position)
    {
        return (byte >> HALFBYTE_SIZE);
    }

    else
    {
        return (byte & HALFBYTE_MASK);
    }
}

/*
 * assertP:
 * function is made for easy module tests of setHalfByte function.
 * INPUT:
 * byte: storage for two halfbytes
 * position: position of halfbyte to change
 * value: new value to store
 * expected: expected result of halfByteStorage() function
 * OUTPUT:
 * printed result of the test
 */
void assertP(uint8_t test_byte, uint8_t position, uint8_t value, uint8_t expected)
{
    ++test_counter;
    uint8_t result = 0;
    setHalfByte(&test_byte, position, value);
    result = getHalfByte(test_byte, position);

    if(result == expected)
    {
        printf("test %d passed\n", test_counter);
    }

    else
    {
        printf("\ntest %d fails\n", test_counter);
        printf("returned: %d\nexpected: %d\n", result, expected);
    }
}
