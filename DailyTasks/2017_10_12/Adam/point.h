#ifndef POINT_H
#define POINT_H


class Point
{
public:
    Point();

    virtual void print() = 0;

    int x() const;
    void setX(int x);

    int y() const;
    void setY(int y);

    virtual char type() const = 0;

    char m_type;

protected:

    int m_x;
    int m_y;

};

#endif // POINT_H
