#include "point.h"
#include <iostream>

Point::Point()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

int Point::x() const
{
    return m_x;
}

void Point::setX(int x)
{
    m_x = x;
}

int Point::y() const
{
    return m_y;
}

void Point::setY(int y)
{
    m_y = y;
}



