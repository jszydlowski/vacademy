#ifndef CROSS_H
#define CROSS_H

#include <iostream>
#include "point.h"

class Cross: public Point
{
public:
    Cross();

    void print();


    char type() const;
};

#endif // CROSS_H
