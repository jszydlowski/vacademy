#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>
#include "point.h"

class Circle: public Point
{
public:
    Circle();

public:
    void print();


    char type() const;
};


#endif // CIRCLE_H
