#include "game.h"

Point* board[3][3];
static int m_moves = 9;

Game::Game()
{

}

Game::~Game()
{

}

void Game::gameStart()
{
    while(1)
    {

        if(m_round == false)
        {
            std::cout <<"X- your turn" << std::endl;
        }
        else
        {
            std::cout <<"O- your turn" << std::endl;
        }

        std::cout << "type x and y: ";
        std::cin >> m_x;
        std::cin >> m_y;
        if(board[m_x][m_y] || m_x > 2 || m_y > 2)
        {
            std::cout << "Point to the right place" << std::endl;
            continue;
        }

        std::cout << "position: "<< m_x <<" " << m_y << std::endl;

        if(m_round == false)
        {
            Cross *ob = new Cross();
            ob->setX(m_x);
            ob->setY(m_y);

            board[m_x][m_y] = ob;
        }
        else
        {
            Circle *ob = new Circle();

            ob->setX(m_x);
            ob->setY(m_y);

            board[m_x][m_y] = ob;
        }
         system("cls");

        if(m_round == true)
        {
            m_round = false;
        }
        else
        {
            m_round = true;
        }

        printBoard();
        if(checkResult())
        {
            exit(0);
        }
    }
}

void Game::printBoard()
{
    for(int i = 0; i < 3; i++)
    {

        for(int j = 0; j < 3; j++)
        {

            if(board[j][i] == nullptr)
            {
                std::cout << " - ";
            }
            else
            {
                board[j][i]->print();
            }

        }
        std::cout << std::endl;
    }
}

bool Game::checkResult()
{
    m_moves--;
    if(m_moves == 0)
    {
        system("cls");
        std::cout << "drow!";
        return 1;
    }

    for(int i = 0; i< 3 ; i++)
    {
        if(board[i][0] && board[i][1] && board[i][2])
        {
            if((board[i][0]->type()) == (board[i][1]->type()) &&
                    (board[i][1]->type()) == (board[i][2]->type()))
            {
                system("cls");
                std::cout << board[i][1]->type() << " won!"<< std::endl;
                return 1;
            }
        }
        else if(board[0][i] && board[1][i] && board[2][i])
        {
            if((board[0][i]->type()) == (board[1][i]->type()) &&
                    (board[2][i]->type() == board[0][i]->type()))
            {
                system("cls");
                std::cout << board[i][1]->type() << " won!" << std::endl;
                return 1;
            }
        }
        else if(board[0][0] && board[1][1] && board[2][2])
        {
            if((board[0][0]->type()) == (board[1][1]->type()) &&
                    (board[2][2]->type()) == (board[0][0]->type()))
            {
                system("cls");
                std::cout << board[1][1]->type() << " won!" << std::endl;
                return 1;

            }
        }
        else if(board[0][2] && board[1][1] && board[2][0] )
        {
            if((board[0][2]->type()) == (board[1][1]->type()) &&
                    (board[2][0]->type())  == (board[1][1]->type()))
            {
                system("cls");
                std::cout << board[1][1]->type() << " won!" << std::endl;
                return 1;

            }
        }
    }
    return 0;
}

