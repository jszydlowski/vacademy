#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <circle.h>
#include <point.h>
#include <cross.h>


class Game
{
public:
    Game();
    ~Game();

    void gameStart();
    void printBoard();
    bool checkResult();

private:
    int m_x;
    int m_y;
    bool m_round = false;


};

#endif // GAME_H
